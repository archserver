#!/usr/bin/lua
------------------------------------
-- author: kylin
-- date: 2008.10.10
------------------------------------

local username = "admin"
local realm = "authorized users only"
local passfile = "passwd"	-- "/etc/lighttpd/.passwd"

local cmd = string.format([[htdigest -c %s '%s' %s]], passfile, realm, username)

os.execute(cmd)

-- cp passwd /etc/lighttpd/.passwd
