CREATE TABLE postfix_alias (
	id int(11) unsigned NOT NULL auto_increment,
	alias varchar(128) NOT NULL default '',
	destination varchar(128) NOT NULL default '',
	PRIMARY KEY (id)
) TYPE=MyISAM;

CREATE TABLE postfix_relocated (
	id int(11) unsigned NOT NULL auto_increment,
	email varchar(128) NOT NULL default '',
	destination varchar(128) NOT NULL default '',
	PRIMARY KEY (id)
) TYPE=MyISAM;

CREATE TABLE postfix_transport (
	id int(11) unsigned NOT NULL auto_increment,
	domain varchar(128) NOT NULL default '',
	destination varchar(128) NOT NULL default '',
	PRIMARY KEY (id),
	UNIQUE KEY domain (domain)
) TYPE=MyISAM;

CREATE TABLE postfix_virtual_domains (
	id int(11) unsigned NOT NULL auto_increment,
	domain varchar(128) NOT NULL default '',
	destination varchar(128) NOT NULL default '',
	PRIMARY KEY (id),
	UNIQUE KEY domain (domain)
) TYPE=MyISAM;

CREATE TABLE postfix_users (
	id int(11) unsigned NOT NULL auto_increment,
	email varchar(128) NOT NULL default '',
	clear varchar(128) NOT NULL default '',
	crypt varchar(128) NOT NULL default '',
	name tinytext NOT NULL,
	uid int(11) unsigned NOT NULL default '2001',
	gid int(11) unsigned NOT NULL default '2001',
	homedir tinytext NOT NULL,
	maildir tinytext NOT NULL,
	quota tinytext NOT NULL,
	access enum('Y','N') NOT NULL default 'Y',
	postfix enum('Y','N') NOT NULL default 'Y',
	disablepop3 char(1) NOT NULL default '0',
	disableimap char(1) NOT NULL default '0',
	disablewebmail char(1) NOT NULL default '0',
	sharedgroup varchar(128) NOT NULL default '0',
	smtpaccess enum('Y','N') NOT NULL default 'Y',

	PRIMARY KEY (id),
	UNIQUE KEY email (email)
) TYPE=MyISAM;

CREATE TABLE postfix_virtual (
	id int(11) unsigned NOT NULL auto_increment,
	email varchar(128) NOT NULL default '',
	destination varchar(128) NOT NULL default '',
	PRIMARY KEY (id)
) TYPE=MyISAM;

CREATE TABLE postfix_access (
	id int(10) unsigned NOT NULL auto_increment,
	source varchar(128) NOT NULL default '',
	access varchar(128) NOT NULL default '',
	type enum('recipient','sender','client') NOT NULL default 'recipient',
	PRIMARY KEY (id)
) TYPE=MyISAM ; 
