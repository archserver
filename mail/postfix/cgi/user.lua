#!/usr/bin/lua
dofile "lib.inc";

function cb_head()
	print([[<script type="text/javascript">
	function do_submit()
	{

	}
	function do_cancel()
	{

	}
	function init()
	{

	}
	</script>]])
end

function cb_table()
	local i = 0;
	local _td01 = "</td><td>";

	--[[rows(con, "select id,email,clear,crypt,name,uid,gid,homedir,maildir,quota,access,"..
		"postfix,disablepop3,disableimap,disablewebmail,sharedgroup,smtpaccess"..
		" from postfix_users")
		]]
	-- XXX do not show some fields
	-- should we design a model to describe database, then will easy when database structure changed
	-- email  quota access postfix disablepop3 disableimap disablewebmail smtpaccess

	print('<tr class="th"><td width="25%">'..lang.email..
		'</td><td width="10%">'	..lang.quota..
		'</td><td width="6%">'	..lang.access..
		'</td><td width="8%">'	..lang.postfix..
		'</td><td width="8%">'	..lang.disablepop3..
		'</td><td width="8%">'	..lang.disableimap..
		'</td><td width="12%">'	..lang.disablewebmail..
		'</td><td width="8%">'	..lang.smtpaccess..
		'</td><td>'		..lang.action..
		'</td></tr>');
	for id,email,clear,crypt,name,uid,gid,homedir,maildir,quota,access,postfix,disablespop3,
		disableimap,disablewebmail,sharedgroup,smtpaccess
	in
		rows(con, "select id,email,clear,crypt,name,uid,gid,homedir,maildir,quota,access,"..
			"postfix,disablepop3,disableimap,disablewebmail,sharedgroup,smtpaccess"..
			" from postfix_users")
	do
		print('<tr class="tr_' .. (i%2) .. '"><td>' ..
			email 		.. _td01 ..
			quota 		.. _td01 ..
			access 		.. _td01 ..
			postfix 	.. _td01 ..
			disablespop3 	.. _td01 ..
			disableimap 	.. _td01 ..
			disablewebmail 	.. _td01 ..
			smtpaccess 	.. _td01 ..
			'</tr>');
		i = i + 1;
	end
end

-- create all element for add/edit in the form
function out_edit_element()
	print('<tr><td width="120px">'..lang.email..'</td><td><input type=text id=email name=email>'..'</td>'
	..'<tr><td>'..lang.quota..'</td><td><input type=text id=quota name=quota>'..'</td>'
	..'<tr><td>'..lang.access..'</td><td><input type=text id=access name=access>'..'</td>'
	..'<tr><td>'..lang.postfix..'</td><td><input type=text id=postfix name=postfix>'..'</td>'
	..'<tr><td>'..lang.disablepop3..'</td><td><input type=text id=dpop3 name=dpop3>'..'</td>'
	..'<tr><td>'..lang.disableimap..'</td><td><input type=text id=dimap name=dimap>'..'</td>'
	..'<tr><td>'..lang.disablewebmail..'</td><td><input type=text id=dwebmail name=dwebmail>'..'</td>'
	..'<tr><td>'..lang.smtpaccess..'</td><td><input type=text id=smtpac name=smtpac>'..'</td>'
	..'<tr><td>&nbsp;</td><td><input type=submit id=submit name=submit onclick="do_submit();" value="'
	..lang.submit..'">&nbsp;<input type=button id=cancel onclick="do_cancel();" value="'
	..lang.cancel..'"></td></tr>');
end

function cb_form()
	as_table(out_edit_element, "100%", "");
	as_table(cb_table, "100%", "");
end

function cb_body()
	print("<b>" .. lang["postfix user management"] .. ":</b><p>");
	db_open();
	--[[
	for email, pwd in rows(con, "select email,clear from postfix_users") do
		print(string.format ("%s:%s<br>", email, pwd));
	end
	]]
	show_errmsg()
	as_form(cb_form, "do_user");
end

-- within framework
as_head(cb_head, lang["postfix user management"]);
as_body(cb_body, "init();");
