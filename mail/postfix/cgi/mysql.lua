#!/usr/bin/lua
dofile "lib.inc";
-- variables declare
env, con = nil, nil;

function cb_head()
	print([[<script type="text/javascript">
	function init()
	{

	}
	</script>]])
end

function db_open()
	require "luasql.mysql";
	env = assert (luasql.mysql())
	con = assert (env:connect(CONF_DB.DBN, CONF_DB.USR, CONF_DB.PWD, CONF_DB.SRV), "mysql connect bad")
end

function rows(connection, sql_statement)
	local cursor = assert(connection:execute(sql_statement))
	return function()
		return cursor:fetch()
	end
end

function cb_body()
	db_open();
	for email, pwd in rows(con, "select email,clear from postfix_users") do
		print(string.format ("%s:%s<br>", email, pwd))
	end
end

-- within framework
as_head(cb_head, "mysql as_header");
as_body(cb_body, "init();");
