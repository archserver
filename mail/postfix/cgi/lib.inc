-- library for cgi
if _lib_inc_init ~= nil then return end
_lib_inc_init = 1
io.stderr = io.stdout
dofile("config.cnf");
dofile("lang_" .. CONF_LANG .. ".inc");

err_msg = nil	-- show err_msg
-- archss_header output
-- title: outout the web title
-- fun_cb: callback function for private data
function as_head(fun_cb, title)
	assert(type(title) == "string");
	assert(type(fun_cb) == "function");

	print('Content-type: text/html\n\n' ..
		'<HTML><HEAD><TITLE>' .. title .. '</TITLE>\n' ..
		'<META HTTP-EQUIV="Content-Type" CONTENT="text/html; CHARSET="UTF-8"/>\n' ..
		'<LINK REL=STYLESHEET HREF="styles.css" TYPE="TEXT/CSS">');
	fun_cb();
	print('</HEAD>');
end

-- onload: web init with javascript
function as_body(fun_cb, onload)
	assert(type(onload) == "string");
	assert(type(fun_cb) == "function");

	print('<BODY ONLOAD="' .. onload .. '">');
	fun_cb();
	print('</BODY></HTML>');
end

-- archss_form output
-- target: post this form to which cgi
-- datatype: used for binary or normal data
function as_form(fun_cb, target, datatype)
	assert(type(fun_cb) == "function")
	assert(type(target) == "string")

	local dt = "";
	if datatype ~= nil then dt = datatype end;
	print('<form action="' .. target .. CONF_EXT .. '" method="post"' .. dt .. '>');
	fun_cb();
	print('</form>');
end

function as_table(fun_cb, width, class)
	assert(type(fun_cb) == "function")
	assert(type(width) == "string")

	print('<table width="' .. width .. '" style="' .. class .. '">');
	fun_cb();
	print('</table>\n')
end

-- parse STDIN and query_string
local _as_stdin_init;	-- parsed flag
local _as_query_data = {};	-- query data store
--local _as_kv_token = "(%w+)=([%w_-%%]+)"
local _as_kv_token = "([^&=]+)=([^&=]+)"
-- FIXME this MUST work with multi-byte encoded data
function QUERY()
	if _as_stdin_init ~= nil then return _as_query_data end
	local query_string = os.getenv("QUERY_STRING");
	local k, v

	if query_string == nil then return _as_query_data end
	for k, v in string.gfind(query_string, _as_kv_token) do
		_as_query_data[unescape(k)] = unescape(v);
	end
	_as_stdin_init = 1;
	return _as_query_data;
end

local _as_post_init;
local _as_post_data = {};	-- post data store
function POST()
	if _as_post_init ~= nil then return _as_post_data end
	local read = io.read();
	local k, v

	if read == nil then return _as_post_data end
	for k, v in string.gfind(read, _as_kv_token) do
		_as_post_data[unescape(k)] = unescape(v);
	end
	_as_post_init = 1;
	return _as_post_data;
end

--[[ all key/value in one
   FIXME how to do it
function QP_ENV()
	return QUERY() .. POST();
end
]]

function show_errmsg()
	if err_msg ~= nil then print('<div class="err_msg">'..err_msg..'</div>') end
end

-- database deal with
env, con = nil, nil;
function db_open()
	require "luasql.mysql";
	env = assert (luasql.mysql())
	con = assert (env:connect(CONF_DB.DBN, CONF_DB.USR, CONF_DB.PWD, CONF_DB.SRV), "mysql connect bad")
end

function rows(connection, sql_statement)
	local cursor = assert(connection:execute(sql_statement))
	return function()
		return cursor:fetch()
	end
end

-- form deal with
function unescape (s)
	if s == nil then return s end
	s = string.gsub(s, "+", " ")
	s = string.gsub(s, "%%(%x%x)", function (h)
		  return string.char(tonumber(h, 16))
		end)
	return s
end

function escape (s)
	s = string.gsub(s, "([&=+%c])", function (c)
		  return string.format("%%%02X", string.byte(c))
		end)
	s = string.gsub(s, " ", "+")
	return s
end

function encode (t)
	local s = ""
	local k, v
	for k,v in pairs(t) do
		s = s .. "&" .. escape(k) .. "=" .. escape(v)
	end
	return string.sub(s, 2)     -- remove first `&'
end


-- private msg store with "XXX_ARCHSS", we need config it when install
-- vim:ft=lua
