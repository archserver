lang = {
[""] = "",
["postfix user management"] = "邮件系统用户管理",
["postfix notdefined"] = "postfix",

["id"] = "编号",
["email"] = "邮件地址",
["clear"] = "明文密码",
["crypt"] = "加密密码",
["name"] = "名字",
["uid"] = "用户ID",
["gid"] = "组ID",
["homedir"] = "家路径",
["maildir"] = "邮件存放路径",
["quota"] = "限额",
["access"] = "启用",
["postfix"] = "POSTFIX",
["disablepop3"] = "禁用pop3",
["disableimap"] = "禁用imap",
["disablewebmail"] = "禁用webmail",
["sharedgroup"] = "共享组",
["smtpaccess"] = "smtp允许",

["action"] = "动作",
["add"] = "增加",
["del"] = "删除",
["edit"] = "修改",
["submit"] = "增加",
["cancel"] = "取消",

}
-- vim:ft=lua
