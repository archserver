lang = {
[""] = "",
["postfix user management"] = "postfix user management",
["postfix notdefined"] = "postfix",

["id"] = "id",
["email"] = "email",
["clear"] = "clear",
["crypt"] = "crypt",
["name"] = "name",
["uid"] = "uid",
["gid"] = "gid",
["homedir"] = "homedir",
["maildir"] = "maildir",
["quota"] = "quota",
["access"] = "access",
["postfix"] = "postfix",
["disablepop3"] = "disablepop3",
["disableimap"] = "disableimap",
["disablewebmail"] = "disablewebmail",
["sharedgroup"] = "sharedgroup",
["smtpaccess"] = "smtpaccess",

["action"] = "action",
["add"] = "add",
["del"] = "delete",
["edit"] = "edit",
["submit"] = "submit",
["cancel"] = "cancel",

}
-- vim:ft=lua
