#!/bin/sh
echo -e "Content-type: text/html\n\n"

source lib.sh

sambadir=$rootdir/samba

h_header
h_global_nav shares


 $sambadir/samba/list_users.sh
 $sambadir/samba/list_shares.sh
 $sambadir/samba/connect_users.sh
h_footer
