#!/bin/sh
echo -e "Content-type: text/html\n\n";

this_page="shares.manager_user"
source lib.sh
source ./proccgi.sh

admin_user=0
sambadir=$rootdir/samba

####
# Funcions
####
function create_user() {
						[ "$F_passwd" != "$F_passwd_cf" ] && echo "<h1>Password and confirmed password not matched !</h1>" && return
						# add samba user 
						if echo $F_user_name | egrep '\<admin\>|\<administrator\>'; then
								useradd_arg="-o -u 0"
								admin_user=1
						fi
						sudo useradd -m $useradd_arg $F_user_name
						ret=$?
						[ $ret != 0 ] && grep "useradd_err_$ret" $sambadir/samba/useradd.err|sed 's/.*=//' && return

						(echo $F_passwd; echo $F_passwd) |sudo smbpasswd -L -s -a $F_user_name 
						[ $admin_user = 1 ] && return
						sudo $sambadir/modify_samba_config.pl /etc/samba/smb.conf $F_user_name /home/$F_user_name $F_user_name yes
						echo "<h1>Add user \"$F_user_name\" OK!</h1>"
}

function update_user() {
				[ "$F_passwd" != "$F_passwd_cf" ] && echo "<h1>Password and confirmed password not matched !</h1>" && return
				if sudo cat /etc/samba/private/smbpasswd | cut -f1 -d: | egrep "\<$F_user_name\>"; then
						(echo $F_passwd; echo $F_passwd) |sudo passwd -s -a $F_user_name
						echo 'Changed password Succesfully! "'
						return
				fi
				echo "User $F_user_name not exist, Please create it."
}

# del samba user
function delete_user() {
		sudo smbpasswd -x $F_delname >/dev/null
		sudo $sambadir/modify_samba_config.pl /etc/samba/smb.conf $F_delname

		sudo userdel -r $F_delname 2>/dev/null
		ret=$?
		[ $ret != 0 ] && grep "userdel_err_$ret" $sambadir/samba/userdel.err|sed 's/.*=//' && return
		echo "<h1>Delete user \"$F_delname\" OK!</h1>"
		return
}

####
# Html Start
####
h_header 

#[ "$F_todo" = "" ] &&( $rootdir/shares/manager_user.lua;  exit)

$rootdir/shares/manager_user.lua 
h_footer
exit
# Some checks
[ $F_todo != "delete" -a "$F_user_name" = "" ] && h_err_quit "<font color=red>Please enter username</font>" 
[ $F_todo != "delete" -a "$F_passwd" = ""  ] && h_err_quit "<font color=red>Please enter password</font>" 
[ $F_todo != "delete" -a "$F_passwd_cf" = ""  ] && h_err_quit "<font color=red>Please enter confirm password</font>" 
[ $F_todo = "delete" -a "$F_delname" = "" ] && h_err_quit "<font color=red>Please enter username</font>" 

# Create user
[ "$F_todo" = "create" ] && create_user

# Update User
[ "$F_todo" = "update" ] && update_user

# Delete user
[ "$F_todo" = "delete" ] && delete_user

h_footer
