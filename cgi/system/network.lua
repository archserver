#!/usr/bin/lua

dofile "libutil.lua"

-- retun table {mode(dhcp,static), ip, netmask, broadcast, gw, dns}
function get_network()
				local _tbl = {}
				local _t = myread("/etc/rc.conf")
				_,_,_s = string.find(_t, "\neth0=(.-)\n")
				if (string.find(_s, "dhcp")) then
								_tbl["mode"] = "dhcp"
								_s = myexec("ifconfig eth0")
								string.gsub(_s, "inet addr:(.-)%s*Bcast:(.-)%s*Mask:(.-)\n", function(ip,bc,nm)
												_tbl["ip"] = ip
												_tbl["netmask"] = nm
												_tbl["broadcast"] = bc
								end)
								_s = myexec("route -n")
							  _,_,_s = string.find(_s, "\n0.0.0.0%s*\t*(.-)%s*\t*0.0.0.0")
								_tbl["gateway"] = _s
								_s = myread("/etc/resolv.conf")
								_tbl["dns"]=""
								string.gsub(_s, "nameserver%s+(.-)\n", function(v)
												_tbl["dns"] = _tbl["dns"].." "..v
								end)


				else
								_tbl["mode"] = "static"
								string.gsub(_s, "eth%d%s*(.-)%s*netmask%s*(%S+%d)", function(ip,nm)
												_tbl["ip"] = ip
												_tbl["netmask"] = nm
--												_tbl["broadcast"] = bc
								end)
								_,_,_s = string.find(_t, "\ngateway=\"default gw (.-)\"\n")
								if _s then _tbl["gateway"]=_s else _tbl["gateway"]="" end
								_tbl["dns"]=""
								_s = myread("/etc/resolv.conf")
								string.gsub(_s, "nameserver%s*(%S+)", function(v)
												_tbl["dns"] = v.." ".._tbl["dns"] 
								end )
				end
				return _tbl
end

function set_network(tbl)
				local _current_net_tbl = get_network()
				local _t = tbl
				if (_t.mode == _current_net_tbl.mode) then print("equal mode") return end
				if _t.mode == "dhcp" then
								file_gsub("/etc/rc.conf", "(\neth0=.-\n)", "\neth0=\"dhcp\"\n")
								file_gsub("/etc/rc.conf", "(\ngateway=)", "\n#gateway=")
								myexec("sudo /usr/bin/killall dhcpcd; sudo /sbin/dhcpcd eth0")
				else
--								local _ip= "eth0 ".._t.ip.." netmask ".._t.netmask.." broadcast ".._t.broadcast
								local _ip= "eth0 ".._t.ip.." netmask ".._t.netmask
								local _gw= "default gw ".._t.gateway
								file_gsub("/etc/rc.conf", "(\neth0=.-\n)", "\neth0=\"".._ip.."\"\n")
								file_gsub("/etc/rc.conf", "(\n#gateway.-\n)", "\ngateway=\"".._gw.."\"\n")
								myexec("sudo /sbin/ifconfig ".._ip)
								myexec("sudo /sbin/route add -net ".._gw)
								local _s = myread("/etc/resolv.conf")
								_s = string.gsub(_s, "nameserver .-\n", "")
								for _dns in string.gmatch(_t.dns, "%S+") do
												_s = string.gsub(_s, "$", "nameserver ".._dns.."\n")
								end
								mywrite("/etc/resolv.conf", _s)
				end

end

local qp = ui_getqp()
local b = {}

if qp.todo then
				set_network(qp)
else
				b = get_network()
				if b.mode == "dhcp"  then
								r_dhcp = "dhcp checked"
								r_static= "static"
				else
								r_dhcp = "dhcp"
								r_static= "static checked"
				end

print([[<h3>网络设置</h3>
				<form action="system_network.cgi">
				<div class="table_input_body">

				<table cellspacing="2" cellpadding="8" border="0" width="100%">

				<tr><td class=color_table_heading>Mode</td>
				<td class=color_table_row2>
				<input type=radio align=left name="mode" value=]]..r_dhcp..[[>自动获得 IP 地址
				<input type=radio name="mode" value=]]..r_static..[[>使用下面的 IP 地址</td></tr>

				 <tr>
				  <td class=color_table_heading >IP 地址</td><td class=color_table_row1 >
					<input type=text name="ip" value=]]
					..b.ip..

				[[></td></tr>
				<tr><td class=color_table_heading>子网掩码</td>
				<td class=color_table_row2>
				<input type=text name="netmask" value=
				]]
				..b.netmask..

				[[></td></tr>	
				<tr><td class=color_table_heading>默认网关</td>
				<td class=color_table_row1>
				<input type=text name="gateway" value=
				]]
				..b.gateway..

				[[></td></tr>	
				<tr><td class=color_table_heading>DNS 服务器</td>
				<td class=color_table_row2>
				<input type=text name="dns" value=
				]]
				..b.dns..

				[[></td></tr>	
				</table></div>
				<input type=hidden name="todo" value=todo>
				<input type=submit value="提交">
				</form>]])

end
