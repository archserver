#!/usr/bin/lua

dofile "libutil.lua"

function display()
	print([[
		<form action="system_shutdown.cgi" method="post">
		<input type="hidden" name="todo" value="e">
		<table cellpadding="8" cellspacing="2" border="0">
		<tr>
		<td class="color_table_heading" valign="top" align="left"><strong>希望服务器做什么</strong></td>
		<td class="color_table_row1" valign="top" align="left"><input type="radio" name="todo" value="halt" checked="checked" />&nbsp;停止服务器<br />
		<input type="radio" name="todo" value="reboot" />&nbsp;重启服务器</td>
		</tr>
		<tr>
		<td class="color_table_heading" valign="top" align="left"><strong>重启/关机前延迟时间</strong></td>
		<td class="color_table_row2" valign="top" align="left"><input type="text" name="delay" size="3" value="0" />&nbsp;&nbsp;分钟</td>
		</tr>
		<tr>
		<td colspan="2" align="center"><input type="submit" value="重启/关机" /></td>
		</tr>
		</table>
		</form>
	]])
end
local qp = ui_getqp()
print("<h3> 重启/关闭 服务器</h3>")
if not qp.todo then
				display()
else
				local cmd = ""
				local err, ret
				if qp.todo == "halt" then 
								cmd = "sudo /sbin/shutdown -h" 
				else 
								cmd = "sudo /sbin/shutdown -r"
				end
				cmd = cmd.." +"..qp.delay.." &"

				err, ret = myexec(cmd)
				if ret ~= 0 then ui_message_err(err) end
end
