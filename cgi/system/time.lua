#!/usr/bin/lua

dofile "libutil.lua"

function display()
				local time = os.date("*t", os.time())
				print([[
        <h3 >设置系统时间</h3>
		<div align="center">
		<form action="system_time.cgi" method="post">
		<input type="hidden" name="todo" value="newdatetime" />
		<table cellpadding="8" cellspacing="2" border="0">
		<tr>
			<td class="color_table_heading" align="right"><strong>日期:</strong></td>
				<td class="color_table_row1">
					<select name="year">
					]])

	for year=2008,2050 do 
					if year==tonumber(time.year) then
									print([[<option value="]]..year..[[" selected="selected">]]..year..[[</option>]])
					else
									print([[<option value="]]..year..[[">]]..year..[[</option>]])
					end
	end
	if tonumber(time.year) < 2008 then
									print([[<option value="]]..time.year..[["selected="selected">]]..time.year..[[</option>]])
	end
	print([[
						</select>
							<select name="month">
							]])

	for month=1,12 do 
					m = string.format("%02d", month)
					if month==tonumber(time.month) then
									print([[<option value="]]..m..[[" selected="selected">]]..m..[[</option>]])
					else
									print([[<option value="]]..m..[[">]]..m..[[</option>]])
					end
	end
	print([[</select><select name="day">]])

	for day=0,31 do 
					d = string.format("%02d", day)
					if day==tonumber(time.day) then
									print([[<option value="]]..d..[[" selected="selected">]]..d..[[</option>]])
					else
									print([[<option value="]]..d..[[">]]..d..[[</option>]])
					end
	end

	print([[
				</select>
				</td>
				</tr>
				<tr>
				<td class="color_table_heading" align="right"><strong> 时间:</strong></td>
				<td class="color_table_row2">
				<select name="hour">
				]])
	for hour=0,23 do 
					local h = string.format("%02d", hour)
					if hour==tonumber(time.hour) then
									print([[<option value="]]..h..[[" selected="selected">]]..h..[[</option>]])
					else
									print([[<option value="]]..h..[[">]]..h..[[</option>]])
					end
	end
	print([[
															</select>
																<strong>:</strong>
																	<select name="minute">
																	]])
	for min=0,59 do 
					local m = string.format("%02d", min)
					if min==tonumber(time.min) then
									print([[<option value="]]..m..[[" selected="selected">]]..m..[[</option>]])
					else
									print([[<option value="]]..m..[[">]]..m..[[</option>]])
					end
	end
	print([[
		</select>
			</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit" name="actionnewdatetime" value="设置时间日期" /></td>
				</tr>
				</table>
				</form>
				</div>
				]])
end

function settime(t)
				local time = t.month..t.day..t.hour..t.minute..t.year
				local err,ret
				err,ret = myexec("sudo /bin/date "..time)
				if ret ~= 0 then ui_message_err(err) return nil end
				print("设置时间 ... <font color=green><strong>成功</strong></font>")
end
local qp = ui_getqp()
if not qp.todo then 
				display()
else
				settime(qp)
end

