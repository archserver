-- library for cgi
if _lib_inc_init ~= nil then return end
_lib_inc_init = 1
io.stderr = io.stdout


-- parse STDIN and query_string
local _as_stdin_init;	-- parsed flag
local _as_query_data = {};	-- query data store
--local _as_kv_token = "(%w+)=([%w_-%%]+)"
local _as_kv_token = "([^&=]+)=([^&=]+)"
-- FIXME this MUST work with multi-byte encoded data
function QUERY()
	if _as_stdin_init ~= nil then return _as_query_data end
	local query_string = os.getenv("QUERY_STRING");
	local k, v

	if query_string == nil then return _as_query_data end
	for k, v in string.gfind(query_string, _as_kv_token) do
		_as_query_data[unescape(k)] = unescape(v);
	end
	_as_stdin_init = 1;
	return _as_query_data;
end

local _as_post_init;
local _as_post_data = {};	-- post data store
function POST()
	if _as_post_init ~= nil then return _as_post_data end
	local read = io.read();
	local k, v

	if read == nil then return _as_post_data end
	for k, v in string.gfind(read, _as_kv_token) do
		_as_post_data[unescape(k)] = unescape(v);
	end
	_as_post_init = 1;
	return _as_post_data;
end

--[[ all key/value in one
   FIXME how to do it
function QP_ENV()
	return QUERY() .. POST();
end
]]

function show_errmsg()
	if err_msg ~= nil then print('<div class="err_msg">'..err_msg..'</div>') end
end

-- database deal with
env, con = nil, nil;
function db_open()
	require "luasql.mysql";
	env = assert (luasql.mysql())
	con = assert (env:connect(CONF_DB.DBN, CONF_DB.USR, CONF_DB.PWD, CONF_DB.SRV), "mysql connect bad")
end

function rows(connection, sql_statement)
	local cursor = assert(connection:execute(sql_statement))
	return function()
		return cursor:fetch()
	end
end

-- form deal with
function unescape (s)
	if s == nil then return s end
	s = string.gsub(s, "+", " ")
	s = string.gsub(s, "%%(%x%x)", function (h)
		  return string.char(tonumber(h, 16))
		end)
	return s
end

function escape (s)
	s = string.gsub(s, "([&=+%c])", function (c)
		  return string.format("%%%02X", string.byte(c))
		end)
	s = string.gsub(s, " ", "+")
	return s
end

function encode (t)
	local s = ""
	local k, v
	for k,v in pairs(t) do
		s = s .. "&" .. escape(k) .. "=" .. escape(v)
	end
	return string.sub(s, 2)     -- remove first `&'
end

function ui_getqp()
--				local _qt = QUERY()
--				local _pt = POST()
				local _qpt = {}
--				local _k, _v
--				for _k,_v in pairs(_qt) do _qpt["\"".._k.."\""] = _v end
--				for _k,_v in pairs(_qt) do _qpt["\"".._k.."\""] = _v end
				
				local _t = myexec("set")
				string.gsbu(_t, "(.-)=(.-)\n", function(n,v) 
								n=tostring(n) 
								if(string.sub(n,1,2) == "F_") then _tbl[n] = v	end	
				end)
				return _qpt
end

-- private msg store with "XXX_ARCHSS", we need config it when install
-- vim:ft=lu
--
