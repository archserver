#!/bin/sh

echo -e '
		<form action = "volumes_create_raid.cgi" method = "post">

		<h3>1. 选择阵列类型: </h3>
		<div class="messageblock messageblock-info">
		<div class="messageblock-internal">
			<p> Select a Raid type in the list below.</p>
		</div></div> 选择阵列类型：
		<select name="raid_method">
		<option value="no">无阵列</option>
		<option value="raid0">Raid-0</option>
		<option value="raid1">Raid-1</option>
		<option value="raid5">Raid-5</option>
		<option value="raid6">Raid-6</option>
		<option value="raid10">Raid-10</option>
		</select>
		<br><br><br>
		'

sudo /sbin/fdisk -l | grep 'Disk /dev'  | sed 's:.*\(/dev.*GB\).*:\1:' >/tmp/disks.tmp


echo -e '
		<h3>2. 选择可用磁盘:</h3>
		<div class="messageblock messageblock-info">
		<div class="messageblock-internal"><p>
		创建Raid-0, Raid-1 至少要两块硬盘. Raid-5 需要3块硬盘. Raid-6, Raid-10至少需要4块硬盘.<br>
		选中的硬盘将被格式化,数据将被破坏.
		</p></div></div>
		<br><br>

		<table cellspacing="2" cellpadding="8" border="0" width="95%">
		<tr align="center"><td bgcolor="#ebebeb"><strong>硬盘</strong></td><td bgcolor="#ebebeb"><strong>容量</strong></td><td bgcolor="#ebebeb"><strong>型号</strong></td><td bgcolor="#ebebeb"><strong>状态</strong></td></tr>
		'

count=1
 while read line
  do
		  dev=$(echo $line | cut -f1 -d:)
		  size=$(echo $line | cut -f2 -d:)
		  model=$(sudo /sbin/hdparm -I $dev| grep 'Model Number' | cut -f2 -d:  | tr -s ' ')
			let _n=$count%2+1
			if  df -h | grep -q $dev; then
							echo "<tr bgcolor="#c6d5a8" align="center"><td><input type=\"checkbox\" name=\"devs_$count\" value=\"$dev\">
										$dev </td><td>$size</td><td>$model</td><td><font color=red>"正在使用"</font></td></tr>"
		elif cat /proc/swaps | grep -q $dev; then
							echo "<tr bgcolor="#c6d5a8" align="center"><td><input type=\"checkbox\" name=\"devs_$count\" value=\"$dev\">
										$dev </td><td>$size</td><td>$model</td><td><font color=red>"使用\(swap\)"</font></td></tr>"
		else
				  echo "<tr bgcolor="#c6d5a8" align="center"><td><input type=\"checkbox\" name=\"devs_$count\" value=\"$dev\">
		  			$dev </td><td>$size</td><td>$model</td><td>"未使用"</td></tr>"
		  fi

		  let count=$count+1
  done < /tmp/disks.tmp
  echo "</table><br><br><hr>"



echo -e '
	    <br><h3>3. 选择热备盘</h3>

		<div class="messageblock messageblock-info">
		<div class="messageblock-internal">
			<p> 热备盘是未被使用的空闲硬盘。在Raid阵列中若有磁盘失效，系统会自动用热备盘替换，以确保数据安全性.<br>
			提示：热备盘必须是空闲硬盘，上面创建Raid的硬盘不能被使用.</p>
		</div></div>
		<select name="hot_spare" >
		'
devs="<option value=\"no\">无热备盘</option>"
 while read line
  do
		  dev=$(echo $line | cut -f1 -d:)
		  devs="$devs <option value=$dev>$dev</option>"
  done < /tmp/disks.tmp
  echo $devs
  echo "</select>"

  echo -e '
	<br><br><br><hr>
    <input type="submit" value="创建阵列")
	</form>
	'
  rm -f /tmp/disks.tmp
