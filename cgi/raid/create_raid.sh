#!/bin/sh

source lib.sh
ERR=" 2>&1 || exit"

all_disks="$F_devs_1 $F_devs_2 $F_devs_3 $F_devs_4 $F_devs_5 $F_devs_6 $F_devs_7 $F_devs_8 $F_devs_9 $F_devs_10 $F_devs_11 $F_devs_12 $F_devs_13 $F_devs_14 $F_devs_15 " 

echo -e "all: $all_disks<br>"
echo -e "hotspare: $F_hot_spare<br>"
echo -e "Raid Method: $F_raid_method<br>"

if [ "$F_raid_method" = "no" ]; then
		h_error "Error: No Raid Type Selected."; 
elif [ "$all_disks" = "" ]; then
		h_error "Error: No Disks Selected."; 
fi

disk_nu=$(echo $all_disks | wc -w)
echo -e "Disks: $all_disks<br>"
echo "disk number: $disk_nu<br>"


####################################################
#				 Some check with the parameters ! 				 #
####################################################

m=$F_raid_method
if [ "$m" = "raid0" -a $disk_nu -lt 2 ] || [ "$m" = "raid1" -a $disk_nu -lt 2 ]; then
	h_error "Error: $m need 2 disk at lease"; 
elif [ "$m" = "raid5" -a $disk_nu -lt 3 ]; then 
	h_error "Error: $m need 3 disk at least"; 
elif [ "$m" = "raid6" -a $disk_nu -lt 4 ] || [ "$m" = "raid10" -a $disk_nu -lt 4 ]; then 
	h_error "Error: $m need 4 disk at least"; 
fi

exit
# check whether spare disk is used.
for h in `echo -e $F_hot_spare` 
do
		if echo -e $all_disks | grep -q $h; then
				h_error -e $F_hot_spare" exist in disks!"; 
		fi
done

#check whether DISK is mounted now.
for d in `echo -e $all_disks`
do
				if df | grep -q $d; then
								mdisk=`df | grep $d | awk '{print $NF}'`
								h_error "$d is mounted on \"$mdisk\""
				elif grep -q $d /proc/swaps; then
								h_error "$d is mounted on Swap"
				fi
done

exit
all_disks="/dev/sdb"
####################################################
#						 Create Raid 													 #
####################################################

echo -e "<br>正在创建阵列...<br><font color=red>
		sudo /sbin/mdadm -C /dev/md0 -R -l`echo $m|sed 's/raid//'`  
		`echo -e $all_disks|sed 's/ /2 /g'|sed 's/$/2/g'` `[ "$F_hot_spare" = "no" ] || echo "-x $F_hot_spare"` >/tmp/xxxx.err 2>&1 || 
		h_error $(cat /tmp/xxxx.err; rm /tmp/xxxx.err) 
		<br>"
		
		echo -e '
eval echo "正在格式化阵列为 XFS 文件系统...<br>"
eval eval sudo /sbin/mkfs.xfs -f /dev/md0  $E
eval echo "正在把挂栽阵列到系统上...<br>"
eval sudo /bin/mkdir /tmp/tempdir $E
eval sudo /bin/mount /dev/md0 /tmp/tempdir $E
eval sudo /bin/cp -rdf --preserve /home/* /tmp/tempdir $E
eval sudo /bin/umount /dev/md0 $E
eval sudo /bin/mount /dev/md0 /home $E
#FIXME: found a simple way to operate with /etc/fstab
if ! grep -q "/dev/md0" /etc/fstab; then 
				sudo /bin/cat /etc/fstab > /tmp/fstab 
				sudo /bin/echo "/dev/md0 /home xfs defaults 0 0" 2>&1 >> /tmp/fstab 
				sudo /bin/mv /tmp/fstab /etc/fstab
fi
		<br>'

# Formmat each disk to two partitions: 
# 		1. linux-swap,256MB 2. xfs, all left size.
#for d in `echo -e $all_disks`
#do
#				echo "正在为硬盘[ $d ]分区 ...<br>"
#				disksize=`sudo parted $d p | grep '^Disk /dev/.*' | cut -f 3 -d" "`
#				sudo parted -s $d mklabel msdos 2>&1 || exit
#				sudo parted -s $d mkpart primary linux-swap 0 512 2>&1 || exit
#				sudo parted -s $d mkpart primary xfs	512 $disksize 2>&1 || exit
#				d1=$d"1"
#				sleep 2	# wait for parted completed.
#				sudo /sbin/mkswap -L swap $d1 2>&1 >/dev/null || exit
#				sudo swapon $d1  2>&1 
#				#FIXME: found a simple way to operate with /etc/fstab
#				if ! grep -q "$d1" /etc/fstab; then 
#								sudo cat /etc/fstab > /tmp/fstab 
#								sudo echo "$d1 swap swap defaults 0 0" 2>&1 >> /tmp/fstab 
#								sudo mv /tmp/fstab /etc/fstab
#				fi
#done

echo "Raid $F_raid_method 创建成功！"
