#!/usr/bin/lua

dofile "libutil.lua"

t = myexec("sudo fdisk -l")
dt = {}	-- device table

string.gsub(t, "\n(/.-)\n", function(l)
				repeat 
				if (string.find(l, "Ext") or string.find(l, "extended")) then break end	-- filter "extended" partition
				string.gsub(l, "(.-)%s+%**%s+%d+%s+%d+%s+(%d+).-%x+%s+(.-)$", function(d, s, f)
								dt[d] = {}
								dt[d]["size"] = size2hum(s..'k') 
								dt[d]["fs"] = f 
							--	print("dev: "..d.."\tsize: "..size2hum(s.."K").."\tfilesystem: "..f)
				end)
				until true
end)

print([[
		<form action = "volumes_create_raid.cgi" method = "post">
		<table cellspacing="1" cellpadding="2" border="0" width="60%">
		<tr align="left"><td bgcolor="#ebebeb"><strong>Partition</strong></td><td bgcolor="#ebebeb"><strong>Size</strong></td><td bgcolor="#ebebeb"><strong>Filesystem</strong></td></tr>
		]])

i = 0
for d in pairs(dt) do
				v = one_two(v)
				i = i + 1
				_td1 = string.format([[<tr bgcolor=#c6d5a8 align="left"><td>
				<input type="checkbox" name="devs_%d" value="%s">%s</td>]], v, i, d, d)
				print(_td1)
				print("<td>"..dt[d].size.."</td>")
				print("<td>"..dt[d].fs.."</td></tr>")
end

print([[</table>
				<input type="hidden" name=todo value="todo">
				<input type="submit" value="创建阵列">
				<input type="reset" value="清除">
				</form>]])
