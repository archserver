
-- return output & return value
function myexec(cmd)
   cmd = cmd..[[ 2>&1; echo -e "\nKylin_RETURN$?"]]
   local _pfile = assert(io.popen(cmd))
   local _s = _pfile:read("*all")
   _pfile:close()
   _,_,_s, _r= string.find(_s, "(.*)\nKylin_RETURN(%d+)")
   return _s, tonumber(_r)
end

----------------------------------------------
function myread(file)
   local _f = assert(io.open(file, "r"))
   local _t = _f:read("*all")
   _f:close()
   return _t
end

function mywrite(file, s)
   local _tn = os.tmpname()
   local _tf = assert(io.open(_tn, "w"))
   _tf:write(s)
   _tf:close()
   assert(0 == os.execute("sudo /bin/cp ".._tn.." "..file), "[Error] - ".."cp ".._tn.." "..file)
   return os.execute("rm ".._tn)
end

function read_file_lines(filename)
   local t = myread(filename)
   local lines = {}
   string.gsub(t, "(.-[\n$])", function(l) table.insert(lines,l) end)
   return lines
end

function flush_file_lines(filename, lines)
   mywrite(filename, table.concat(lines))
end

function one_two(v)
   if (v == 1) then 	return 2 	end
   return 1
end

function sec2string(t)
   sec = assert(tonumber(t))
   if (sec < 60 ) then 
      return sec.."秒" 
   elseif (sec < 60*60 ) then 
      return string.format("%d分%d秒", sec/60, sec%60)
   elseif (sec < 86400) then 
      return string.format("%d小时%d分%d秒", sec/(3600), (sec%3600)/60,sec%3600%60)
   else 
      return string.format("%d天%d小时%d分%d秒",sec/(86400), (sec%86400)/(3600), 
                           (sec%86400%3600)/60, sec%86400%3600%60)
   end
end

function time2date(t)
   return string.format("%02d:%02d:%02d/%4d年%02d月%02d日", 
                        t["hour"],t["min"],t["sec"],t["year"],t["month"],t["day"])
end

function size2hum(size)
   s = assert(tonumber(string.sub(size, 1, -2)), "size2hum: Not number")
   u = string.lower(string.sub(size, -1, -1))
   assert(u == "b" or u == "k" or u == "m", [[size2hum: Unit not 'b' or 'k' or 'm']])
   
   if u  == 'b' then 
      if (s < 1024 ) then return s.."Byte" 
      elseif (s < 1048576 ) then return string.format("%.2f", s/1024).." Kb"
      elseif (s < 1073741824 ) then return string.format("%.2f", s/1024/1024).." MB"
      elseif (s < 1073741824*1024) then return string.format("%.2f", s/1024/1024/1024).." GB"
      else return string.format("%.2f", s/1024/1024/1024/1024).."TB"
      end
   elseif u == 'k' then
      if (s < 1024 ) then return s.." Kb" 
      elseif (s < 1048576 ) then return string.format("%.2f", s/1024).." Mb"
      elseif (s < 1073741824 ) then return string.format("%.2f", s/1024/1024).." GB"
      else return string.format("%.2f", s/1024/1024/1024).." TB"
      end
   elseif u == 'm' then
      if (s < 1024 ) then return s.." Mb" 
      elseif (s < 1048576 ) then return string.format("%.2f", s/1024).." Gb"
      else return string.format("%.2f", s/1024/1024).." TB"
      end
   end
end

-- do string.gsub on a file instead of on a string
function file_gsub(file, s, g)
   local _f = assert(io.open(file, "r"))
   local _t = _f:read("*all")
   _t = string.gsub(_t, s, g)
   _f:close()

   local _tn = os.tmpname()
   local _tf = assert(io.open(_tn, "w"))
   _tf:write(_t)
   _tf:close()

   assert(0 == os.execute("sudo /bin/cp ".._tn.." "..file), "[Error] - ".."cp ".._tn.." "..file)
   os.execute("rm ".._tn)
end

-----------------------------------------------------------------------------
-- check with the email string is valid
--
--@param email           the email string
--@return                the email string or false
-----------------------------------------------------------------------------
function is_email_valid(email) 
   return string.match(email,"^[%a][%w%.%-]*[%a%d]*@[%a%d][%w%.%-]*[%a%d]%.[%a][%a]?[%a]?[%a]$")
end
-----------------------------------------------------------------------------
-- CGI utilities 
-----------------------------------------------------------------------------

-- message box
function message_err(s)
   print([[<div class="messageblock messageblock-error">
   <div class="messageblock-internal">
   <p><ul><strong>出现了下面的错误</strong><li>]]..s..[[
   </li><ul></p>
   </div></div>]])
end

function ui_message_err(s)
   print([[<div class="messageblock messageblock-error">
   <div class="messageblock-internal">
   <p><ul><strong>出现了下面的错误</strong><li>]]..s..[[
   </li><ul></p>
   </div></div>]])
end

function ui_message_warning(s)
   print([[<div class="messageblock messageblock-warning">
   <div class="messageblock-internal">
   <p>]]..s..[[
   </p>
   </div></div>]])
end

function ui_message_info(s)
   print([[<div class="messageblock messageblock-info">
   <div class="messageblock-internal">
   <p>]]..s..[[
   </p>
   </div></div>]])
end
function ui_message_success(s)
local success = " ... <font color=green><strong>成功</strong></font><br>"
print(s..success)
end

function ui_redblue_img(red, blue) 
   wred = red*300/(red+blue)
   wred = string.format("%d", wred)
   wblue = 300 - wred
   return "<img src=images/red.gif width="..wred.." height=10><img src=images/blue.gif width="..wblue.." height=10>" 
--  return [[<img width="1" hspace="0" height="8" border="0" alt="disk usage" src="images/backgrounds/bar-start.gif"/><img width="]] ..wred.. [[" hspace="0" height="8" border="0" alt="disk usage" src="images/backgrounds/bar-active-2-2.gif"/><img width="8" hspace="0" height="]] ..wblue.. [[" border="0" alt="disk usage" src="images/backgrounds/bar-inactive.gif"/><img width="1" hspace="0" height="8" border="0" alt="score" src="images/backgrounds/bar-stop.gif"/>]]
end

function ui_myexec(cmd)
   local _s, _r
   _s, _r= myexec(cmd)
   if _r ~= 0 then 	ui_message_err(_s) return nil end
   return _s 
end

function ui_getqp()
   local _tbl = {}
   local _t = myexec("set")
   string.gsub(_t, "(.-)=(.-)\n", function(n,v) 
      n=tostring(n) 
      if(string.sub(n,1,2) == "F_") then _tbl[string.sub(n,3,-1)] = v	end	
   end)
   return _tbl
end

-----------------------------------------------------------------------------
--
-- Network utility 
--
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- LVM and Raid utility 
-----------------------------------------------------------------------------

-- return a table {["vgname"]}.{["size"], ["pvs"]}
function get_vg()
   local lvm = {}

   local fstr = myexec("sudo /sbin/vgdisplay")
   for str in string.gmatch(fstr, "(---%s*Volume group.-\n)%s*\n") do
      string.gsub(str, "VG%s*Name%s*(%S+).-VG%s*Size%s*(.-)\n.-Total%s+PE%s+(%d+)\n", function(v, s, pe)
         lvm[v] = {["size"] = s, ["pvs"] = "", ["totalpe"] = pe}
      end)

      fstr = myexec("sudo /sbin/pvdisplay")
      string.gsub(fstr, "PV%s+Name%s+(%S+)\n.-VG%s+Name%s+(%S+)\n", function(p,v)
         lvm[v]["pvs"] = lvm[v].pvs.." "..p
      end)
   end

   return lvm
end

function is_vg_exist(vgname)
   local vg = get_vg()
   for k,v in pairs(vg) do
      if k==vgname then 	return true end
   end
   return nil
end

function get_lv()
   t = myexec("/bin/df /mnt")
   local lvtbl = nil
   string.gsub(t, "(%S+)%s*(%S+)%s*(%S+)%s*(%S+)%s*(%S+)%s*/mnt", function(p,t,u,a,c)
      lvtbl = {["fs"] = p,
          ["total"] = t,
          ["used"] = u,
          ["free"] = a,
          ["percentage"] =c }
   end)
   if lvtbl then return lvtbl else return nil end
end

function display_lv()
   local lv = get_lv()
   if not lv then print("something wrong: no logic volume") return end

   print([[
         <div class="sysinfo">
         <table class="box">
         <tr class="boxheader">
         <td class="boxheader">共享信息</td>
         </tr>
         <tr class="boxbody">
         <td class="boxbody">
         <table border="0" width="100%" align="center">
         ]])
   print([[<tr valign="top"><td><strong>共享卷使用</strong></td><td>]]
      ..ui_redblue_img(lv.used,lv.free)..
      "( 已使用"..size2hum(lv.used..'k').." / 共"..size2hum(lv.total..'k')..")<strong>  空闲 "..size2hum(lv.free..'k').."</strong></td></tr>")
   print("</table></td></tr></table>")
end

-- return table {["md"]}.{"active", "level", "devices", "size", "err"}
function get_mdstat() 
  local fstr = myexec("cat /proc/mdstat")
   local mdstat = {}
   for str in string.gmatch(fstr, "(md%d%s*:.-\n)%s*\n") do
      local _tbl
      string.gsub(str, "(md%d+)%s*:%s*(%S+)%s*(%S+)%s*(.-)\n%s*(%d+)%s*blocks%s*(.-)\n",	function(m,a,l,d,s,e) 
         mdstat["/dev/"..m] = {["active"]=a,
            ["level"] = l,
            ["devices"] =d,
            ["size"] = s,
            ["err"] = e}
      end)  
      string.gsub(str,"(md%d+)%s*:.-resync%s*=%s*([0-9%.]+).*$", function(m, y) 
            mdstat["/dev/"..m]["resync"] = y
      end)

   end

   for k,v in pairs(mdstat) do
      mdstat[k]["size"] = size2hum(mdstat[k]["size"].."k")
      mdstat[k]["devices"] = string.gsub(mdstat[k]["devices"], "(%S+)%[.-%]", "/dev/%1")
   end
   return mdstat
end

-- return table: {["device"] = ["filesystem"]}
function get_mtab()
   local fstr = myexec("cat /etc/mtab")
   local mtab = {}
   string.gsub(fstr, "(/dev/%S+)%s+(/%S*)", function(d,f)
               mtab[d] = f
   end)
   return mtab
end

-- return table {["device"] }. {["size"],["vendor"],["model"]}
function _get_physical_disks()
   local fstr = myexec("sudo /sbin/fdisk -l")

   local disktbl = {}
   string.gsub(fstr, "Disk%s+(/dev/%S+):%s+(.-),", function(d, s)
      disktbl[d] = {["size"] = s}
      local istr = myexec("sudo /usr/bin/sginfo "..d)
      string.gsub(istr, "Vendor:%s*(%S.-)\nProduct:%s+(%S.-)\n", function(v,p)
         disktbl[d]["vendor"] = v
         disktbl[d]["model"] = p
      end)
   end)
   return disktbl
end


-- return table {["device"] }. {["size"],["vendor"],["model"], ["used"]}
function _get_block_devices(mtab, vg, raid, ttype)
   local disks = _get_physical_disks()
   for d in pairs(mtab) do
      local d1=string.match(d, "(%S+)%d")
      if d1 then
         for d2 in pairs(disks) do
            if d1 == d2 then disks[d1]["used"] = "fs" end
         end
      end
   end
   for m in pairs(raid) do
      for	d1 in string.gmatch(raid[m].devices, "(%S+)%d") do
         if d1 then
            for d2 in pairs(disks) do
               if d1 == d2 then disks[d1]["used"] = "raid" end
            end
         end
      end
   end

   for v in pairs(vg) do
      for	d1 in string.gmatch(vg[v].pvs, "(%S+)%d") do
         if d1 then
            for d2 in pairs(disks) do
               if d1 == d2 then disks[d1]["used"] = "lvm" end
            end
         end
      end
   end

   if ttype == "b" then
      for m in pairs(raid) do
         disks[m] = {
            ["size"] = raid[m].size,
            ["devices"] = raid[m].devices,
            ["model"] = raid[m].level
            }
         for v in pairs(vg) do
            for	d1 in string.gmatch(vg[v].pvs, "(%S+%d+)") do
               if d1 == m then disks[m]["used"] = "lvm" end
            end
         end
   end
   end
   return disks
end

-- return physical disks table {["device"] }. {["size"],["vendor"],["model"], ["used"]}
function get_disks()
   local mtab = get_mtab()
   local vg = get_vg()
   local raid = get_mdstat()
   return _get_block_devices(mtab, vg, raid, "d")

end

-- return block device table {["device"] }. {["size"],["vendor"],["model"], ["used"]}
function get_block_devices()
   local mtab = get_mtab()
   local vg = get_vg()
   local raid = get_mdstat()
   return _get_block_devices(mtab, vg, raid, "b")
end

-- parted a disk to 2 partitions: 1.swap and 2.xfs, then mount swap,write to fstab
function do_disk(disk)
   local success = "<font color=green><strong>成功</strong></font><br>"
   local fstr = myexec("sudo /usr/sbin/parted "..disk.." p")
   local disksize = string.match(fstr, "\nDisk%s+"..disk..":%s+(%S+)\n")
   print("<br>disksize: "..disksize)
   if not disksize then ui_message_err("Not get disk size") return end
   local cmd = {
      ["label"] = "sudo /usr/sbin/parted -s "..disk.." mklabel msdos",
      ["swap"] = "sudo /usr/sbin/parted -s "..disk.." mkpart primary linux-swap 0 512",
      ["xfs"] = "sudo /usr/sbin/parted -s "..disk.." mkpart primary xfs 512 "..disksize
   }
   local err,ret
   local conf = disk.."1\tswap\tswap\tdefaults\t0\t0\n"

   print("<br>正在为磁盘 "..disk.."分区")
   myexec("sudo /sbin/swapoff "..disk.."1")
   err, ret = myexec(cmd.label)
   if ret ~= 0 then ui_message_err(err) return nil end
   err, ret = myexec(cmd.swap)
   if ret ~= 0 then ui_message_err(err) return nil end
   err, ret = myexec(cmd.xfs)
   if ret ~= 0 then ui_message_err(err) return nil else print(success) end

   file_gsub("/etc/fstab", "\n"..disk.."1.-\n", "\n")
   err, ret = mywrite("/etc/fstab", myread("/etc/fstab")..conf)
   myexec("sudo /sbin/mkswap "..disk.."1 && sudo /sbin/swapon "..disk.."1")
   return true
end


-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- ##############  SAMBA Utilities #################
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
smbshare = {}
smbshare.config = "/etc/samba/smb.conf"
smbshare.version = "# Generated by smbshare util.\n# Author: kang, 2008-09-26\n# e-mail: kkmm99@gmail.com\n"

-- return a table from smb.conf: {share,} = {name = value,}
function smbshare.get()
   local t = myread(smbshare.config)
   t = string.gsub(t, "\n%s*#[^\n]+", "\n")
   t = string.gsub(t, "\n%s*;[^\n]+", "\n")
   t = string.gsub(t, "\n+", "\n")				--remove multi lines to one line
   t = string.gsub(t, "%[", "\n%[")			-- make an tag "\n\n" at every end of a share
   t = string.gsub(t, "$", "\n\n");			-- make sure there is a tag at end of file

   tb={}
   string.gsub(t, "%[(.-)%]%s*\n(.-\n)\n", function(sh, p)
      tb[sh] = {}
      string.gsub(p, "%s*(.-)%s*=%s*(.-)%s*\n", function(n,v)
         tb[sh][n] = v
      end)
   end)
   return tb
end

-- write the table to smb.conf
function smbshare.put(tb)
   smbconf = smbshare.version.."[global]\n"
   for n, v in pairs(tb.global) do
      smbconf = smbconf.."\t"..n.." = "..v.."\n"
   end

   for s,k in pairs(tb) do 
      if s ~= "global" then
         smbconf = smbconf.."["..s.."]\n"
         for n, v in pairs(tb[s]) do
            smbconf = smbconf.."\t"..n.." = "..v.."\n"
         end
      end
   end
   mywrite(smbshare.config, smbconf)
end

-- return a template share table
function smbshare.new()
   return {["path"] = "/tmp", 
            ["comment"] = "no comment", 
            ["writable"] = yes, 
            ["create mask"] = 666, 
            ["directory mask"] = 777, 
            ["readlist"] = "", 
            ["writelist"] = ""
            }
end

-- ADD or set a share, and write to smb.conf
function smbshare.set(share, path, readlist, writelist, comment)
   local conftable = smbshare.get()
   local tb = smbshare.new()
   
   tb["path"] = path
   tb["readlist"] = ","..readlist
   tb["writelist"] = ","..writelist
   tb["comment"] = comment
   conftable[share] = tb
   smbshare.put(conftable)
end

-- REMOVE a share from smb.conf
function smbshare.remove(share)
   local conftable = smbshare.get()
   local t = {}
   for s, v in pairs(conftable) do
      if s ~= share then t[s] = v end
   end
   smbshare.put(t)
end

-- return a local user table: {user,} = {uid,gid,comment,home,shell}
function get_local_user()
local fstr = myread("/etc/passwd")
local tbl = {}
string.gsub(fstr, "[%^\n](%S+):%S+:(%d+):(%d+):(.-):(%S+):([^\n]+)",
   function(u,uid,gid,c,h,s)
      if tonumber(uid) >= 1000 then
      tbl[u] = {["uid"] = uid, ["gid"] = gid, ["comment"] = c, ["home"] = h, ["shell"] = s }
      end
   end)
return tbl
end
-- return a samba user table: {user = uid, }
function get_samba_user()
   local fstr = myexec("sudo /bin/cat /etc/samba/private/smbpasswd")
   local tbl = {}
   string.gsub(fstr, "([%w_]+):(%d+)[^\n]+", function(u,uid) tbl[u] = uid end)
   return tbl
end
-- smbshare.set("a new share", "/root/seconf", "nouser", "nogroup", "ok, comment")
-- smbshare.remove("a new share")

-- vim:ts=3 ss=3 sw=3 expandtab
