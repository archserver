#!/usr/bin/lua

dofile "libutil.lua"

-- infotable
it = {}

-------------------------- Hardware -------------
io.input("/proc/meminfo")
t = io.read("*all")
string.gsub(t, "MemTotal.-(%d+).-MemFree.-(%d+).-SwapTotal.-(%d+).-SwapFree.-(%d+)", 
				function(mem,mfree,swap,swapfree) 
								local _img=ui_redblue_img(mem-mfree, mfree)
								muse = mem - mfree
								if (muse > 1024*1024) 
								then muse = string.format("%.2fGB", (mem-mfree)/1024/1024)
								else muse = string.format("%.2fMB", (mem-mfree)/1024)
								end

								if (tonumber(mem) > 1024*1024)
								then 	mem = string.format("%.2fGB", mem/1024/1024)
								else 	mem = string.format("%.2fMB", mem/1024)
								end
								it["j物理内存"] = _img.."(已使用"..muse.." / 共"..mem..")"
				---------------- Swap -----------
								local _img=ui_redblue_img(swap-swapfree, swapfree)
								muse = swap - swapfree
								if (muse > 1024*1024) then muse = string.format("%.2fGB", (swap-swapfree)/1024/1024)
								else muse = string.format("%.2fMB", (swap-swapfree)/1024)
								end

								if (tonumber(swap) > 1024*1024) then 	swap = string.format("%.2fGB", swap/1024/1024)
								else 	swap = string.format("%.2fMB", swap/1024)
								end
								it["k虚拟内存"] = _img.."(已使用"..muse.." / 共"..swap..")"
				end)
io.input("/proc/cpuinfo")
t = io.read("*all")
string.gsub(t, "model name.-: (.-)\n", function(v) it["f处理器"] = v end)
string.gsub(t, "cache size.-: (.-)\n", function(v) 	it["g处理器缓存"] = v end)


-------------------------- System -------------
it["a系统型号"] = "AS100 Version 0.01"
it["b系统名称"] = myexec("hostname")
t = myread("/proc/uptime")
string.gsub(t, "(%d+)%.%d+ ", function(s) 
				it["d目前系统时间"] = "<strong>"..time2date(os.date("*t",os.date(os.time()))).."</strong>".." (启动时间: <strong>"..time2date(os.date("*t", os.time()-s)).."</strong>)"
				it["e已开机时间"]=sec2string(s) 
end)

t = myexec("ifconfig eth0")
string.gsub(t, "inet addr:(.-)%s+Bcast.-RX.-%((.-)%).-%((.-)%)", function(ip,rx, tx)	
		it["cIP地址"] = ip 
		it["n网络流量"] = "流入:<strong> "..rx.."</strong> / 流出:<strong> "..tx.."</strong>"
		end)
									
-------------------------- Network -------------
-------------------------- Disks -------------
print([[<div class="sysinfo">
		<table class="box">
		<tr class="boxheader">
		<td class="boxheader">系统信息</td>
		</tr>
		<tr class="boxbody">
		<td class="boxbody">
		<table border="0" width="100%" align="center">
		]])
a = {}
for n in pairs(it) do table.insert(a, n) end
table.sort(a)
for _,v in ipairs(a) 
		do print([[<tr valign="top"><td><strong>]]..string.sub(v,2,-1).."</strong></td><td>"..it[v].."</td></tr>") end

print("</table></td></tr></table>")

