#!/bin/sh

rootdir=$(pwd)

global_nav_active=`echo $this_page|cut -d. -f1`
sub_nav_active=`echo $this_page|cut -d. -f2`

# Global navigation viriables chinese name
global_navs="status system volumes shares"
#global_navs="status accounts system volumes quota shares services "
zh_status="系统信息"
zh_system="系统管理"
zh_volumes="磁盘管理"
zh_quota="磁盘配额"
zh_shares="共享管理"
zh_services="服务"
zh_accounts="账户管理"

# Sub navigation: [ shares ]
sub_nav_shares="share_info manager_user list_connects "
sub_nav_shares_title="共享设置"
zh_share_info="共享管理"
zh_list_users="所有用户"
zh_list_shares="所有共享"
zh_list_connects="连接状态"
zh_manager_user="用户管理"
zh_edit_share="编辑共享"

# Sub navigation: [ volumes ]
sub_nav_volumes="create_sharelv create_raid vstatus "
sub_nav_volumes_title="磁盘管理"
zh_create_sharelv="共享卷管理"
zh_create_raid="磁盘阵列管理"
zh_vstatus="查看系统磁盘"

# Sub navigation: [ System ]
sub_nav_system="hostname network time shutdown"
sub_nav_system_title="系统管理"
zh_hostname="设置主机名"
zh_network="设置网络"
zh_time="设置系统时间"
zh_shutdown="重启系统/关机"

# Sub navigation: [ Status ]
sub_nav_status="sysinfo"
sub_nav_status_title="系统状态"
zh_sysinfo="系统信息"


function h_header () {
echo -e '
				<html>
				<head>
				  <meta http-equiv="Content-Type" content=text/html; charset="utf-8">
					<link rel=stylesheet href="css.css">
				</head>
				<body>

					<!--Top -->
					<div id="logodiv" class="openfiler-logo">
									 <div id="globalButtons">
										<ul>
												<li class="logout"><a href="/account/logout.html">退出</a></li>
												<li class="status"><a href="status.cgi">系统信息</a></li>
												<li class="shutdown"><a href="system_shutdown.cgi">关机</a></li>
										</ul>
									 </div>
					</div>

					<div id="slidercontainer"><div id="slideDownDiv"><div class="show" id="slidingpaneldiv"></div></div></div> 
					<p>  </p>


					<br>

					<table cellpadding="20" cellspacing="0" border="0" width="100%">
					<tr>
					<td valign="top">
					<div align="center" class="container">

					<!-- global_nav start -------------------------------------------------->
								<table  cellpadding="0" cellspacing="0" border="0" width="100%">
								<tr>
						<td><div id="global_nav" class="global_nav"><ul>
						'

						for nav in `echo $global_navs`
						do
										zh_name=$(eval echo "$""zh_"$nav)
										if [ "$global_nav_active" = "$nav" ]; then
														echo -e "
													<li class=\"active\"><span class=\"$nav\">&nbsp;</span><span style=\"display: block; float: left;\">	<a class=\"selected\" href=\"$nav.cgi\">$zh_name</a></span></li>\n"
									else
													printf "<li><span class=\"%s\">&nbsp;</span><a href=\"%s.cgi\">%s</a></li>\n" $nav $nav $zh_name

									fi
					done

					echo -e '
								</ul></div>
								</td> 
								</tr>
								</table>
					<!-- global_nav end-------------------------------------------------->
				<table cellpadding="5" cellspacing="1" border="0" width="100%">
				<tr>	
				<!--body left start  td valign="top" ------------------------------------------------------------------>

					<td valign="top">
				<div class="shadow">
				<div class="shadow_tr">
				<div class="shadow_bl">
				<div class="shadow_tl">
				<div style="padding-bottom: 20px; padding-left: 5px; padding-right: 5px;  min-height: 1%;"> 
				<div><p>&nbsp;</p></div>
				<div align="center">
				'
	}


###
# Function footer(global_nav_active, sub_nav_active)
###

function h_footer() {
cat << EOB_footer1
				</div></div></div></div></div>
				</td>
				<!--body left end ------------------------------------------------------------------>

				<!--sub nav & support nav start ------------------------------------------------------------------>
				<td valign="top" style="width: 200px;"> <!-- nav links -->
				<div class="shadow"><div class="shadow_tr"><div class="shadow_bl"><div class="shadow_tl">
				<div style="padding-bottom: 10px; padding-left: 5px; padding-right: 5px; padding-top: 5px; min-height: 1%;">
					<div id="sub_nav"><div class="sub_nav_title">$(eval echo "$""sub_nav_"$global_nav_active"_title")</div>
						<ul>
EOB_footer1
__t=$(eval echo "$""sub_nav_"$global_nav_active)
for nav in `echo $__t`
do
				# eg: "system_hostname.cgi -> system/hostname.cgi"
				tcgi="href=\"$global_nav_active""_$nav.cgi\""
				#[ ! -f $tcgi ] && tcgi="href=\"$global_nav_active/$nav.cgi\"" 

				[ "$nav" = "$sub_nav_active" ] && tcgi="class=\"selected\" $tcgi"
				cat << EOB_footer_t
				<li class=$nav><a $tcgi>$(eval echo "$""zh_"$nav)</a></li>
EOB_footer_t
done

#							<li class="system_network"><a class="selected" href="/admin/system.html">Network Setup</a></li>
#							<li class="system_ha_cluster"><a href="/admin/system_cluster.html">HA Cluster Setup</a></li>
#							<li class="system_clock"><a href="/admin/system_clock.html">Clock Setup</a></li>
#							<li class="system_ups"><a href="/admin/system_ups.html">UPS Management</a></li>
#							<li class="system_shutdown"><a href="/admin/system_shutdown.html">Shutdown/Reboot</a></li>
#							<li class="system_notification"><a href="/admin/system_info.html">Notification</a></li>
#							<li class="system_update"><a href="/admin/system_view_update.html">System Update</a></li>
#							<li class="system_backup"><a href="/admin/system_backup.html">Backup/Restore</a></li>
#							<li class="system_console"><a href="/admin/system_shell.html">Secure Console</a></li>
				echo "		</ul>
				</div></div></div></div></div></div>"

#				<div class="shadow"><div class="shadow_tr"><div class="shadow_bl"><div class="shadow_tl">
#				<div style="padding-bottom: 10px;  padding-left: 5px; padding-right: 5px; padding-top: 5px; min-height: 1%;">
#				<div id="support_nav"><div class="sub_nav_title">Support resources</div><ul>
#							 <li class="support_bug"><a href="javascript:popupPage('/admin/support.html?page=support_report');">Report bug</a></li>
#					<li class="support_get"><a href="javascript:popupPage('/admin/support.html?page=support_get');">Get support</a></li>
#					<li class="support_forums"><a href="javascript:popupPage('/admin/support.html?page=support_forums');">Forums</a></li>
#					<li class="support_admin_guide"><a href="javascript:popupPage('/admin/support.html?page=support_admin_guide');">Admin Guide</a></li></ul>
#				</div></div></div></div></div></div>

cat << EOB_footer2
				</td>
				<!--sub nav & support nav start ------------------------------------------------------------------>


				</tr>
				</table>


				</div>
				</td>
				</tr>
				</table>

				<hr>
				<p  align="center">
				&copy; 2007 - 2008 <a href="http://www.****.com/">***科技</a>. 保留一切权力.<br />
				</p>
				</body>
				</html>
EOB_footer2
				#<a href="/">Home</a> &middot; <a href="http://www.openfiler.com/buy/administrator-guide">Documentation</a> &middot; <a href="http://www.openfiler.com/support/">Support</a> &middot; <a href="http://www.openfiler.com/">Website</a> &middot; <a href="http://www.openfiler.com/about/license">License</a> &middot; <a href="/account/logout.html">Log Out</a>
}

function h_err_quit () {
cat << EOB
		<div class="messageblock messageblock-error">
		<div class="messageblock-internal">
		<p><ul><strong>出现了下面的错误</strong><li>  $@</li><ul></p>
		</div></div>
EOB
  h_footer
	exit
}

function h_error() {
cat << EOB
		<div class="messageblock messageblock-error">
		<div class="messageblock-internal">
		<p><ul><strong>出现了下面的错误</strong><li>$@</li><ul></p>
		</div></div>
EOB
	exit
}
