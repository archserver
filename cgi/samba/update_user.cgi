#!/bin/sh
echo -e "Content-type: text/html\n\n";
source ./proccgi.sh

echo "User: $F_user_name<br>"
echo "Pass: $F_passwd<br>"

[ "$F_user_name" = "" ] && echo "<h1>Please enter username</h1>" && exit
[ "$F_passwd" = "" ] && echo "<h1>Please enter password</h1>" && exit

if sudo cat /etc/samba/private/passwd | cut -f1 -d: | egrep "\<$F_user_name\>"; then
		(echo $F_passwd; echo $F_passwd) |sudo passwd -s -a $F_user_name
		echo "Changed password to: \"$F_passwd\""
		exit
fi

echo "User $F_user_name not exist, Please create it."
