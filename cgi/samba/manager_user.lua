#!/usr/bin/lua
local htmlcode = [[
<form name="manager_user" method="post" action="shares_manager_user.cgi">
				<h3>用户管理</h3>
				<table cellspacing="0" cellpadding="0" border="0">
								<tr><td><div class="table_input_head">
								添加/修改用户
								</div></td></tr>
								<tr><td align="left" valign="top">
								<div class="table_input_body">
												<table cellspacing="2" cellpadding="8" border="0" width="100%">
																<tr><td class="color_table_heading" align="right">用户名</td>
																				<td class="color_table_row1">
																				<input type="text" name="user_name">
																</td><tr>
																<tr><td class="color_table_heading" align="right">密码</td>
																				<td class="color_table_row2">
																				<input type="password" name="passwd">
																</td></tr>
																<tr><td class="color_table_heading" align="right">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																确认密码</td>
																				<td class="color_table_row1">
																				<input type="password" name="passwd_cf">
																</td></tr>
												</table>
								</div>
								</td></tr>
				</table>
				<input type=hidden name="todo" value="no">
				<input type="button" onclick="mysubmit('create')" value="创建新用户">
				<input type="button" onclick="mysubmit('update')" value="更改用户密码 ">
				<input type="reset" value="清空">
</form>

<form  name="delete_user" method="post" action="shares_manager_user.cgi">
				<table cellspacing="0" cellpadding="0" border="0">
								<tr><td><div class="table_input_head">
								删除用户
								</div></td></tr>
								<tr><td align="left" valign="top">
								<div class="table_input_body">
												<table cellspacing="2" cellpadding="8" border="0" width="100%">
																<tr><td class="color_table_heading" align="right">
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												删除用户名</td>
																				<td class="color_table_row1">
																				<input type="text" name="delname">
																</td></tr>
												</table>
								</div>
								</td></tr>
				</table>
				<input type=hidden name="todo" value="no">
				<input type="button"  onclick="delsubmit('delete')" value="删除用户">
				<input type="reset" value="清空">
</form>
<hr>
<script type="text/javascript">
function mysubmit(todo) {
				var cf=document.manager_user;
				cf.todo.value=todo;
				cf.submit();
}
function delsubmit(todo) {
				var cf=document.delete_user;
				cf.todo.value=todo;
				cf.submit();
}
</script>
]]

 print(htmlcode)
