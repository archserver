#!/bin/sh

declare -a smbpid
smbstatus -p | sed /^$/d|sed 1,/^-/d  > /tmp/smbpid

echo -e '<hr size=1>
		 <h3><font color=blue>All Connection Status</color></h3>
		 '
echo "<table cellspacing=\"0\">"
echo "<tr><th>User</th><th>Share</th><th>Pid</th><th>Connect From</th></tr>"
while read line
 do 
		 eval pid=$(echo $line | cut -f1 -d' ')
		 eval smbpid[$pid]=$(echo $line|cut -f2 -d' ');
 done < /tmp/smbpid
 rm -f /tmp/smbpid

smbstatus -S | sed '/^$/d' | sed '1,/^-/d' > /tmp/smbshare

while read line
do
		pid=$(echo $line | cut -f2 -d ' ')
		echo "${smbpid[$pid]} $line" |awk '
		{ print "<tr><td>"$1"</td><td>"$2"</td><td>"$3"</td><td>"$4"</td></tr>" }'
done < /tmp/smbshare

echo "</table>"
rm -f /tmp/smbshare
