#!/bin/sh
echo -e "Content-type: text/html\n\n"

echo -e '
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset="utf-8" />
		<title>Samba Filesharing Management</title>
		<link rel=stylesheet href="table.css">
</head>
</html>
'
rootdir=/srv/http/admin/samba

 $rootdir/samba/list_users.sh
 $rootdir/samba/list_shares.sh
 $rootdir/samba/connect_users.sh
