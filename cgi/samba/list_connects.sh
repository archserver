#!/bin/sh

declare -a smbpid
smbstatus -p | sed /^$/d|sed 1,/^-/d  > /tmp/smbpid

echo -e '<hr size=1>
		 <h3><font color=blue>当前所有连接</color></h3>
		 '
echo '<table cellspacing="2" cellpadding="8" border="0" width="90%">'
#echo "<tr class="color_table_heading"><th>User</th><th>Share</th><th>Pid</th><th>Connect From</th></tr>"
echo "<tr bgcolor=\"#c6d5a8\"><th>用户名</th><th>访问共享名</th><th>进程id</th><th>连接自计算机</th></tr>"
while read line
 do 
		 eval pid=$(echo $line | cut -f1 -d' ')
		 eval smbpid[$pid]=$(echo $line|cut -f2 -d' ');
 done < /tmp/smbpid
 rm -f /tmp/smbpid

smbstatus -S | sed '/^$/d' | sed '1,/^-/d' > /tmp/smbshare 2>/dev/null

count=1
echo -e '<form name="delete_users">'
while read line
do
		[ $_n = 2 ] && _n=1 || _n=2
		pid=$(echo $line | cut -f2 -d ' ')
		echo "$_n ${smbpid[$pid]} $line " |awk '
		{ print "<tr class=color_table_row"$1"><td>"$2"</td><td>"$3"</td><td>"$4"</td><td>"$5"</td></tr>" }'
done < /tmp/smbshare

echo "</form>"
echo "</table>"
rm -f /tmp/smbshare
