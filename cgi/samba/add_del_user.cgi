#!/bin/sh
echo -e "Content-type: text/html\n\n";
source ./proccgi.sh
rootdir=/srv/http/admin/samba
admin_user=0

if [ "$F_deluser" = "" ] ; then	 
		# add samba user 
		if echo $F_adduser | egrep '\<admin\>|\<administrator\>'; then
				useradd_arg="-o -u 0"
				admin_user=1
		fi
		sudo useradd -m $useradd_arg $F_adduser 
		ret=$?
		[ $ret != 0 ] && grep "useradd_err_$ret" $rootdir/samba/useradd.err|sed 's/.*=//' && exit

		(echo $F_smbpasswd; echo $F_smbpasswd) |sudo smbpasswd -L -s -a $F_adduser
		[ $admin_user = 1 ] && exit
		sudo $rootdir/modify_samba_config.pl /etc/samba/smb.conf $F_adduser /home/$F_adduser $F_adduser yes
		echo "<h1>Add user \"$F_adduser\" OK!</h1>"
else
		# del samba user
		sudo smbpasswd -x $F_deluser >/dev/null
		sudo $rootdir/modify_samba_config.pl /etc/samba/smb.conf $F_deluser

		sudo userdel -r $F_deluser 2>/dev/null
		ret=$?
		[ $ret != 0 ] && grep "userdel_err_$ret" $rootdir/samba/userdel.err|sed 's/.*=//' && exit
		echo "<h1>Delete user \"$F_deluser\" OK!</h1>"
fi



#Usage: ./modify_samba_config.pl configfile share [path] [comment] [writable]
