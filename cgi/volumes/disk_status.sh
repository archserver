#!/bin/sh


function one_disk() {
				size=`df $1   | grep '^/' | awk '{print $2" "$3" "$NF}'`
				usage=`df $1 -h | grep '^/' | awk '{print $3" / "$2}'`
				_t=$(echo $size|cut -d" " -f1)
				_u=$(echo $size|cut -d" " -f2)
				_d=$(echo $size|cut -d" " -f3)

				totalsize=300
				let redsize=$_u*$totalsize/$_t
				let bluesize=300-$redsize

				echo "<tr bgcolor=\"#c6d5a8\"><td width=250 align=center>$1</td><td>$_d</td><td>($usage)<br>
								<img src=images/red.gif width=$redsize height=10><img src=images/blue.gif width=$bluesize height=10></td></tr>"
}


echo '<h3><font color=blue>Harddisk Status</color></h3>'
echo '<table cellspacing="2" cellpadding="8" border="0" width="90%">'
echo '<tr align="center"><td bgcolor="#ebebeb"><strong>Device</strong></td><td bgcolor="#ebebeb"><strong>Mounted_on</strong></td><td bgcolor="#ebebeb"><strong>Size</strong></td></tr>'
tmpfile="/tmp/.xxx_mtab.tmp"
rm $tmpfile
cat /etc/mtab|grep '^/'|cut -d" " -f 1 >> $tmpfile
while read dev
do
				one_disk $dev
done < $tmpfile
rm $tmpfile

echo "</table>"
