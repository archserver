#!/usr/bin/lua

dofile "libutil.lua"
local success = "<font color=green><strong>成功</strong></font><br>"

function raid_name( r)
				if r=="raid0" then return "磁盘分块" 
				elseif r == "raid1" then return "镜像冗余"
				elseif r == "raid5" then return "校验冗余"
				elseif r == "raid6" then return "双校验冗余"
				elseif r == "raid10" then return "分块镜像"
				else return "未知类型"
				end
end

function display_current_raid()
				local mdstat = get_mdstat()
				print([[
						<h3>系统已有Raid</h3>
						<form action = "volumes_create_raid.cgi" method = "post">
						<table width=90%><tr><td>
						<table cellspacing="1" cellpadding="5" border="0" width="100%">
						<tr align="left">
						<td bgcolor="#ebebeb" width=1%></td>
						<td bgcolor="#ebebeb"><strong>Raid名称</strong></td>
						<td bgcolor="#ebebeb"><strong>类型</strong></td>
						<td bgcolor="#ebebeb"><strong>容量</strong></td>
						<td bgcolor="#ebebeb"><strong>使用磁盘</strong></td>
						</tr>
						]])


						for r in pairs(mdstat) do
								local _td1 = string.format([[<tr bgcolor=#c6d5a8 align="left"><td>
								<input type="radio" name="md" value="%s"></td><td>%s</td>]], r,string.match(r, "/dev/(%S+)"))
								print(_td1)
								print("<td>"..raid_name(mdstat[r].level).." ("..mdstat[r].level..")</td>")
								print("<td>"..mdstat[r].size.."</td>")
								print("<td>"..mdstat[r].devices.."</td></tr>")
				end

				print([[</table></td></tr>
								<tr><td>
								<input type="hidden" name=todo value="type">
								<input type="submit" value="创建新阵列">
								<input type="hidden" value="清除">
								</td></tr></table>
								</form><br>]])
end

function display_create_raid()
				print([[
				<h3>Raid管理 － 选择Raid类型</h3>
				<form  name="select_raid" method="post" action="volumes_create_raid.cgi">
				<table width=70%><tr><td>
				<table cellspacing="1" cellpadding="5" border="0" width="100%">
								<tr>
								<td bgcolor="#ebebeb" width=1%></td>
								<td bgcolor="#ebebeb"><strong>Raid类型</strong></td>
								<td bgcolor="#ebebeb"><strong>概述</strong></td>
								<td bgcolor="#ebebeb"><strong>备注</strong></td></tr>
				<tr class="color_table_row1"><td ><input type="radio" name="raidtype" value="0"></td>
								<td>Raid0</td><td>磁盘分块</td><td>无冗余</td></tr>
				<tr class="color_table_row2"><td><input type="radio" name="raidtype" value="1"></td>
								<td>Raid1</td><td>镜像冗余</td><td></td></tr>
				<tr class="color_table_row1"><td><input type="radio" name="raidtype" value="5"></td>
								<td>Raid5</td><td>校验冗余</td><td>推荐</td></tr>
				<tr class="color_table_row2"><td><input type="radio" name="raidtype" value="6"></td>
								<td>Raid6</td><td>双校验冗余</td><td></td></tr>
				<tr class="color_table_row1"><td><input type="radio" name="raidtype" value="10"></td>
								<td>Raid10</td><td>分块镜像</td><td></td></tr>
				</table></tr>
				<tr><td><input type="submit" value="选择"></td></tr></table>
				<input type=hidden name="todo" value="disks">
				</form>
				]])
end

function display_disks(qp)
				if not qp.raidtype then ui_message_err("没有选择Raid类型") return end
				print("select: "..qp.raidtype)
				local disks = get_disks()
				print([[
				<h3>Raid管理 － 选择磁盘</h3>
				<form  name="select_raid" method="post" action="volumes_create_raid.cgi">
				<table width=70%><tr><td>
				<table cellspacing="1" cellpadding="5" border="0" width="100%">
								<tr>
								<td bgcolor="#ebebeb" width=1%></td>
								<td bgcolor="#ebebeb"><strong>磁盘编号</strong></td>
								<td bgcolor="#ebebeb"><strong>容量</strong></td>
								<td bgcolor="#ebebeb"><strong>型号</strong></td></tr>
				]])

				i = 0
				for d in pairs(disks) do
						if not disks[d].used then
								i = i + 1
								v = one_two(v)
								print([[<tr class="color_table_row]]..v..[["><td><input type="checkbox"
									name="dev_]]..i..[[" value="]]..d..[["></td><td>]]..i..[[</td><td>]]..disks[d].size..[[
									</td><td>]]..disks[d].model..[[</td></tr>]])
					end
				end
				print([[
				</table></tr>
				<tr><td><input type="submit" value="创建">&nbsp;<input type="reset" value="清空"></td>
				</tr></table>
				<input type=hidden name="todo" value="create">
				<input type=hidden name="raidtype" value="]]..qp.raidtype..[[">
				</form>
				]])
end


-- call mdadm to make a real raid and add to mdadm.conf
function make_raid(raid)
				local err, ret
				local cmd="sudo /sbin/mdadm -C "..raid.md.." -l"..raid.level.." -n"..raid.num.." "..raid.devices.." -R"
				local conf="\nDEVICE "..raid.devices.."\nARRAY "..raid.md.." level=raid"..raid.level.." devices="..string.gsub(raid.devices,"%s+",",").."\n"

				if raid.spare ~= "" then cmd=cmd.." -x "..raid.spare end
				for d in string.gmatch(raid.devices, "(%S+)") do
								if not do_disk(d) then return  end
				end

				print("正在创建磁盘阵列 ...")
				err, ret = myexec(cmd)
				if ret ~= 0 then ui_message_err(err) return nil else print(success) end

				mywrite("/etc/mdadm.conf", myread("/etc/mdadm.conf")..conf)	
end

function create_raid(qp)
				local raid = {["devices"]="", ["num"]=0, ["level"] = -1, ["md"] = "", ["spare"]=""}
				local mdstat = get_mdstat()

				if not qp.raidtype then ui_message_err("No raid type") return end
				for k,v in pairs(qp) do
								if string.sub(k,1,4) == "dev_" then 
												raid.devices = raid.devices.." "..qp[k] 
												raid.num = raid.num + 1
								end
				end
				if raid.devices == "" then ui_message_err("No devices") return end
				raid.level = tonumber(qp.raidtype)	

				for i=0, 9	do 
								 for md in pairs(mdstat) do
												 if "/dev/md"..i ~= md then raid.md="/dev/md"..i break end 
								 end
								 if raid.md ~= "" then break end
				end
				if not make_raid(raid) then return nil end
				print("oK")
end

------------------- CGI OUTPUT ---------------------
local qp = ui_getqp()
if not qp.todo then
				display_current_raid()
--				display_create_raid()
elseif qp.todo == "type" then
				display_create_raid()
elseif qp.todo == "disks" then
				display_disks(qp)
elseif qp.todo == "create" then
				create_raid(qp)
end
