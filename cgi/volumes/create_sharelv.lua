#!/usr/bin/lua

dofile "libutil.lua"
local vgname = "sharevg"
local lvname = "homelv"
local lvmount = "/mnt"

function list_free_disks()
				ui_message_info([[
										<strong>创建共享卷:</strong>下面列出了您系统上的空闲磁盘.
								]])
				print([[
						<form action = "volumes_create_sharelv.cgi" method = "post">
						<table cellspacing="1" cellpadding="5" border="0" width="90%">
						<tr align="left">
						<td bgcolor="#ebebeb"></td>
						<td bgcolor="#ebebeb"><strong>磁盘编号</strong></td>
						<td bgcolor="#ebebeb"><strong>类型</strong></td>
						<td bgcolor="#ebebeb"><strong>容量</strong></td>
						<td bgcolor="#ebebeb"><strong>型号</strong></td>
						</tr>
						]])

				i = 0
--				dt = get_disks()
				dt = get_block_devices()
				for d in pairs(dt) do 
								if not dt[d].used then 
								i = i + 1
								local _status, _used = "<font color=green>空闲</font>", ""
								local _type = "硬盘"
								if string.sub(dt[d].model,1,4) == "raid" then _type = "磁盘阵列" end
								if dt[d].used then 
												_status = "<font color=red>使用中</font>" 
												if dt[d].used == "fs" then
																_used = "文件系统"
												elseif dt[d].used == "lvm" then
																_used = "共享卷"
												elseif dt[d].used == "raid" then
																_used = "磁盘阵列"
												end

								end
								local _td1 = string.format([[<tr bgcolor=#c6d5a8 align="left"><td>
								<input type="checkbox" name="devs_%d" value="%s"></td><td>%s</td>]], i, d, d)
								print(_td1)
								print("<td>".._type.."</td>")
								print("<td>"..dt[d].size.."</td>")
								print("<td>"..dt[d].model.."</td>")
				end
				end

				print([[</table>
								<input type="hidden" name=todo value="todo">
								<input type="submit" value="创建共享卷">
								<input type="reset" value="清除">
								</form><br>]])
end

function display()
				if is_vg_exist(vgname) then
								display_lv()
				else
								ui_message_warning([[
										<strong>创建共享卷:</strong>您的系统上还没有共享卷，共享卷是有一个或多个磁盘组成的虚拟磁盘，所有共享数据都存放在这里.<a href=volumes_create_sharelv.cgi?todo=disks>创建您的共享卷</a>
								]])
				end
end

function create_lv(qp)
				local devices = "" 
				local err = ""
				local vgsize = ""
				local success = "<font color=green><strong>成功</strong></font><br>"
				local conf = "/dev/"..vgname.."/"..lvname.."\t"..lvmount.."\txfs\tdefaults\t0\t0\n"
				print(conf)
				for k,v in pairs(qp) do
						if string.sub(k,1,5) == "devs_" then 
							print(qp[k])
							if string.find(qp[k], "%S+%d+") then
								devices = devices.." "..qp[k] 
							else
								if not do_disk(qp[k]) then ui_message_err("do_disk error") return 
								else
								devices = devices.." "..qp[k].."2"
								end

							end
						end
				end
				if string.len(devices) == 0 then ui_message_err("请选择磁盘") return end

				print("正在创建物理卷 ...")
				err, ret= myexec("sudo /sbin/pvcreate -f -y "..devices)
				if ret ~= 0 then 	ui_message_err(err.."ret "..ret) return else print(success) end

				print("正在创建卷组 ...")
				err, ret= myexec("sudo /sbin/vgcreate "..vgname.." "..devices)
				if ret ~= 0 then 	ui_message_err(err.."ret "..ret) return else print(success) end
				vg = get_vg()
				for v in pairs(get_vg()) do
					if v == vgname then 	
								vgsize = vg[v].totalpe
					end
				end

				print("正在创建共享卷 ...")
				err, ret= myexec("sudo /sbin/lvcreate -n "..lvname.." -l "..vgsize.." "..vgname)
				if ret ~= 0 then 	ui_message_err(err.."ret "..ret) return else print(success) end

				print("正在格式化共享卷 ...")
				err, ret= myexec("sudo /sbin/mkfs.xfs -f ".."/dev/"..vgname.."/"..lvname)
				if ret ~= 0 then 	ui_message_err(err.."ret "..ret) return else print(success) end

				print("正在加载共享卷 ...")
				err, ret= myexec("sudo /bin/mount /dev/"..vgname.."/"..lvname.." /mnt")
				if ret ~= 0 then 	ui_message_err(err) return end
				if ret ~= 0 then 	ui_message_err(err.."ret "..ret) return else print(success) end
				mywrite("/etc/fstab", myread("/etc/fstab")..conf)
				file_gsub("/etc/rc.conf", "USELVM=.-\n","USELVM=\"yes\"")
end

------------------- CGI OUTPUT ---------------------
local qp = ui_getqp()
				print("<h3>共享卷管理</h3>")
if not qp.todo then
				display()
elseif qp.todo == "disks" then
				list_free_disks()
else 
			create_lv(qp)
end
