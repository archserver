#!/usr/bin/lua

io.input("smb.conf")
t = io.read("*all")
t = string.gsub(t, "\n%s*#[^\n]+", "\n")
t = string.gsub(t, "\n%s*;[^\n]+", "\n")
t = string.gsub(t, "\n+", "\n")		--remove multi lines to one line
t = string.gsub(t, "%[", "\n%[")			-- make an tag "\n\n" at every end of a share
t = string.gsub(t, "$", "\n\n");			-- make sure there is a tag at end of file


tb={}
string.gsub(t, "%[(.-)%]%s*\n(.-\n)\n", function(sh, p)
				tb[sh] = {}
				string.gsub(p, "%s*(.-)%s*=%s*(.-)%s*\n", function(n,v)
								tb[sh][n] = v
				end)
end)

print("\n")
for i,k in pairs(tb) do 
				print("[-"..i.."-]")
				for n, v in pairs(tb[i]) do
								print("\t"..n.." = "..v) 
				end
end
