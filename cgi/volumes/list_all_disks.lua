#!/usr/bin/lua

dofile "libutil.lua"

function list_free_disks()
				print([[
					<h3>当前系统磁盘状态</h3>
						<form action = "volumes_create_sharelv.cgi" method = "post">
						<table cellspacing="1" cellpadding="5" border="0" width="90%">
						<tr align="left">
						<td bgcolor="#ebebeb"><strong>磁盘编号</strong></td>
						<td bgcolor="#ebebeb"><strong>类型</strong></td>
						<td bgcolor="#ebebeb"><strong>容量</strong></td>
						<td bgcolor="#ebebeb"><strong>型号</strong></td>
						<td bgcolor="#ebebeb"><strong>状态</strong></td>
						<td bgcolor="#ebebeb"><strong>使用于</strong></td>
						</tr>
						]])

				i = 0
				dt = get_disks()
--				dt = get_block_devices()
				for d in pairs(dt) do
								i = i + 1
								local _status, _used = "<font color=green>空闲</font>", ""
								local _type = "硬盘"
								if string.sub(dt[d].model,1,4) == "raid" then _type = "磁盘阵列" end
								if dt[d].used then 
												_status = "<font color=red>使用中</font>" 
												if dt[d].used == "fs" then
																_used = "文件系统"
												elseif dt[d].used == "lvm" then
																_used = "共享卷"
												elseif dt[d].used == "raid" then
																_used = "磁盘阵列"
												end

								end
								local _td1 = string.format([[<tr bgcolor=#c6d5a8 align="left"><td>%s</td>]], i, d, d)
								print(_td1)
								print("<td>".._type.."</td>")
								print("<td>"..dt[d].size.."</td>")
								print("<td>"..dt[d].model.."</td>")
								print("<td>".._status.."</td>")
								print("<td>".._used.."</td></tr>")
				end

				print([[</table>
								<input type="hidden" name=todo value="todo">
								<input type="hidden" value="创建">
								<input type="hidden" value="清除">
								</form><br>]])
end

list_free_disks()
