#!/usr/bin/lua

dofile "libutil.lua"

function filteruser (all, rm)
	left = ""
	for a in string.gmatch(all, "(%S+)") do
		find = nil
		for r in string.gmatch(rm, "(%S+)") do
			if a == r then find = true end
		end
		if not find then left = left.." "..a end
	end
	return left
end

function display(share,path,comment,readlist, writelist)
	local smbuser, alluser = get_samba_user(), ""
	for u in pairs(smbuser) do 	alluser = alluser.." "..u end

	alluser = filteruser(alluser, readlist.." "..writelist), 

	print([[
	<form method="post" name="myform" action="shares_edit_share.cgi"> 
	<h3>编辑共享</h3>
	<table cellspacing="1" cellpadding="2" border="0" width="40%">
					<tr><td class="color_table_heading" align="right">共享名称</td>
					<td class="color_table_row1">
					<input type="text" name="share" id="_share">
					</td><tr>
					<tr><td class="color_table_heading" align="right">共享目录</td>
					<td class="color_table_row2">
					<input type="text" name="path" id="_path">
					</td></tr>
					<tr><td class="color_table_heading" align="right">
					共享描述</td>
					<td class="color_table_row1">
					<input type="text" name="comment" id="_comment">
					</td></tr>
	</table>
	<hr>
	]])
	print([[
	<script language="JavaScript"> 
	<!-- 
	document.myform.share.value="]]..share..[[";
	document.myform.path.value="]]..path..[[";
	document.myform.comment.value="]]..comment..[[";
	//--> 
	</script> 
		]])

	ui_message_info([[<strong>用户和组访问此共享的权限: </strong>选定一项或多项然后点击添加或移除(按住shift或ctrl可以多选)，或双击进行添加和移除。</p>]]) 

	print([[
	<table border="0" width="50%"> 
	<tr> 
	<td width="30%"> 
	<strong>所有用户和组</strong>
	<select style="width:100%;" multiple name="list1" size="10" ondblclick="moveOption(document.myform.list1, document.myform.list2)"> ]])

	for auser in string.gmatch(alluser, "(%S+)") do
		print([[<option value="]]..auser..[[">]]..auser..[[</option>]])
	end

	print([[
	</select> 
	</td> 
	<td width="5%" align="center"> 
	<input type="button" value="添加" onclick="moveOption(document.myform.list1, document.myform.list2)"><br><br> 
	<input type="button" value="全选" onclick="moveAllOption(document.myform.list1, document.myform.list2)"><br><br> 
	<input type="button" value="删除" onclick="moveOption(document.myform.list2, document.myform.list1)"><br><br> 
	<input type="button" value="全删" onclick="moveAllOption(document.myform.list2, document.myform.list1)"> 
	</td> 
	<td width="30%"> 
	<strong>只读用户和组</strong>
	<select style="width:100%;" multiple name="list2" size="10" ondblclick="moveOption(document.myform.list2, document.myform.list3)"> ]])

	for ruser in string.gmatch(readlist, "(%S+)") do
		print([[<option value="]]..ruser..[[">]]..ruser..[[</option>]])
	end

	print([[
	</select> 
	</td> 
	<td width="5%" align="center"> 
	<input type="button" value="添加" onclick="moveOption(document.myform.list2, document.myform.list3)"><br><br> 
	<input type="button" value="全选" onclick="moveAllOption(document.myform.list2, document.myform.list3)"><br><br> 
	<input type="button" value="删除" onclick="moveOption(document.myform.list3, document.myform.list2)"><br><br> 
	<input type="button" value="全删" onclick="moveAllOption(document.myform.list3, document.myform.list2)"> 
	</td> 
	<td width="30%"> 
	<strong>读写用户和组</strong>
	<select style="width:100%;" multiple name="list3" size="10" ondblclick="moveOption(document.myform.list3, document.myform.list2)"> ]])

	for wuser in string.gmatch(writelist, "(%S+)") do
		print([[<option value="]]..wuser..[[">]]..wuser..[[</option>]])
	end

	print([[
	</select> 
	</td> 
	</tr> 
	</table> 
	<hr>

	<input type=hidden name="readlist" id="readlist">
	<input type=hidden name="writelist" id="writelist">
	<input type=hidden name=todo value="create_share">
	<input type="submit" value="提交设置">
	</form> 

	<script language="JavaScript"> 
	<!-- 

	document.getElementById('readlist').readOnly= true;
	document.getElementById('writelist').readOnly= true;
	document.myform.readlist.value=getvalue(document.myform.list2); 
	document.myform.writelist.value=getvalue(document.myform.list3); 

	function moveOption(e1, e2){ 
			var e3 = document.myform.list3;
			try{ 
			for(var i = 0; i < e1.options.length; i++){ 
			if(e1.options[i].selected){ 
			var e = e1.options[i]; 
			e1.remove(i); 
			e2.options.add(new Option(e.text, e.value)); 
			i = i - 1; 

			} 
			} 
			document.myform.readlist.value=getvalue(document.myform.list2); 
			document.myform.writelist.value=getvalue(document.myform.list3); 
			} 
			catch(e){} 
	} 

	function getvalue(geto){ 
			var allvalue = ""; 
			for(var i = 0; i < geto.options.length; i++){ 
			allvalue += geto.options[i].value + ","; 
			} 
			return allvalue; 
	} 

	function moveAllOption(e1, e2){ 
			var e3 = document.myform.list3;
			try{ 
			for(var i = 0;i < e1.options.length; i++){ 
			var e = e1.options[i]; 
			e2.options.add(new Option(e.text, e.value)); 
			e1.remove(i); 
			i = i - 1; 


			} 
			document.myform.readlist.value=getvalue(document.myform.list2); 
			document.myform.writelist.value=getvalue(document.myform.list3); 
		} 
		catch(e){} 
	} 
	//--> 
	</script> 
	]])
end


--<strong>只读用户和组:</strong><textarea cols="65" rows="2" name="readlist" id="readlist"  value="" /> </textarea><br>
--<strong>读写用户和组:</strong><textarea cols="65" rows="2" name="writelist" id="writelist"  value="" /> </textarea><br>
--
----------------------------------- 
-- CGI OUTPUT 
-----------------------------------
qp = ui_getqp()

local user_list = nil

local share = qp.share or ""
local path = qp.path or ""
local comment = qp.comment or ""
local readlist, writelist = "", ""

if qp.todo =="delete_user" then
	local err,ret,path
	
	if qp.share=="" or qp.path == "" then ui_message_err("共享名称和共享目录不能为空.") return end

elseif qp.todo == "create_user" then
	if true then print ("dfdf") end
	local user_list = get_samba_user()

	if qp.user_name and user_list[qp.user_name] then 
		ui_message_err("用户 <strong>\""..qp.user_name.."\" </strong>已存在") 
		return
	end

	for u in pairs(user_list) do
		print(u.."<br>")
	end

elseif qp.todo == "update_user" then
	local user_list = get_samba_user()

	if not qp.user_name or not user_list[qp.user_name] then 
		ui_message_err("用户 <strong>\""..qp.user_name.."\" </strong>不存在") 
		return 
	end
	
	print("update")
end
