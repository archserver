#!/usr/bin/lua

dofile "libutil.lua"

function display()
	local user_list = get_samba_user()
	local options = "<option id=_select style=\"color:blue;\" value=myselect selected>------ 用户列表 ------</option>"
	local hiddens = [[<input type=hidden id="myselect">]]
	local style = [[style="background-image:url(icons/volume.png);"]]
		
	for u in pairs(user_list) do
			options = options..[[<option ]]..style..[[ value="]]..u..[[">]]..u..[[</option>]]
	end

		print([[
			<h3>用户管理</h3>
			<form action="shares_manager_user.cgi",method="post">
			<table width="80%"><tr><td>
			<select class="icon-menu" name="selTitle" size=10 id="titles" onchange="setOtherText(this)" width="100%"> 
			]]
			..options..
			[[
			</select>
			</td>
			<td>
				<div align="left">
				<table width="90%" cellspacing="2" cellpadding="5" border="0">
				<tr class=color_table_row1><td>
					<strong>用户名称</strong>
					<input type="text" id="user_name" name="user_name"> 
				</td></tr>
				<tr class=color_table_row2><td>
					<strong>用户密码</strong>
					<input type="password" id="passwd" name="passwd"> 
				</td></tr>
				<tr class=color_table_row1><td>
					<strong>确认密码</strong>
					<input type="password" id="passwd_cf" name="passwd_cf"> 
				</td></tr>


				<tr><td>
				<input type=hidden name=todo value="">
				<input type="submit" value="新建用户" onclick="return mysubmit(1)">
				<input type="submit" value="更新用户" onclick="return mysubmit(2)">
				<input type="submit" value="删除用户" onclick="return mysubmit(4)">
				</td></tr>

				<tr class=color_table_row2 id="delete_cf" style="display:none;"><td>
					<strong>删除用户的共享文件夹?</strong>
					<input type="checkbox"  name="delete_sh"> 
					<input type="submit"  value="确认删除" onclick="return mysubmit(4)"> 
				</td></tr>
				</table>
				</div>
			</td></tr>
			</table>
			</form>

			<script> 
			function setOtherText(obj) { 
				if (!document.layers) { 
					var user = document.getElementById("user_name"); 
					var passwd = document.getElementById("passwd"); 
					var passwd_cf = document.getElementById("passwd_cf"); 
					user.value = obj.value;
					if (obj.value == "myselect") {
						user.value = "";
					}
					passwd.value = "";
				} 
			} 

			function mysubmit(value) {
					if (value == 1){
							document.forms[0].todo.value="create_user";
					} else if (value == 2) {
							document.forms[0].todo.value="update_user";
					} else if (value == 3) {
						var del = document.getElementById("delete_cf");
						del.style.display="inline";
						return false;

					} else if (value == 4) {
							var ss = "<input type=checkbox name=d >您要删除用户\""+document.getElementById("user_name").value + "\"吗?";
							if (confirm(ss)) {
								document.forms[0].todo.value="delete_user";
							} else {
								return false;
							}
					}
					document.forms[0].submit();
			}
			</script> 

			]])
end
-------------------------
--CGI OUTPUT 
-------------------------
qp = ui_getqp()

local user_list = get_samba_user()
local err, ret

if not qp.todo then
elseif qp.todo == "create_user" then

	if qp.user_name and user_list[qp.user_name] then 
		ui_message_err("用户 <strong>\""..qp.user_name.."\" </strong>已存在") 
		return
	end

	if qp.username=="" or qp.passwd=="" or qp.passwd_cf=="" then
		ui_message_err("用户名和密码不能为空.") 
		return
	end

	if qp.passwd ~= qp.passwd_cf then
		ui_message_err("密码和确认密码不符.") 
		return
	end

	err,ret = myexec("sudo /usr/sbin/useradd "..qp.user_name)
	if ret ~= 0 then ui_message_err(err) return end
	err,ret = myexec("(echo "..qp.passwd.."; echo "..qp.passwd..")|sudo /usr/bin/smbpasswd -L -s -a "..qp.user_name)
	if ret ~= 0 then ui_message_err(err) return end
	
	ui_message_success("添加用户: "..qp.user_name)

elseif qp.todo == "update_user" then
	if not qp.user_name or not user_list[qp.user_name] then 
		ui_message_err("用户 <strong>\""..qp.user_name.."\" </strong>不存在") 
		return
	end

	if qp.username=="" or qp.passwd=="" or qp.passwd_cf=="" then
		ui_message_err("用户名和密码不能为空.") 
		return
	end

	if qp.passwd ~= qp.passwd_cf then
		ui_message_err("密码和确认密码不符.") 
		return
	end

	err,ret = myexec("(echo "..qp.passwd.."; echo "..qp.passwd..")|sudo /usr/bin/smbpasswd -L -s -a "..qp.user_name)
	if ret ~= 0 then ui_message_err(err) return end
	ui_message_success("更改用户: "..qp.user_name)

elseif qp.todo=="delete_user" then
	if qp.user_name == "" then
		ui_message_err("用户名不能为空.") 
		return
	end

	if not qp.user_name or not user_list[qp.user_name] then 
		ui_message_err("用户 <strong>\""..qp.user_name.."\" </strong>不存在") 
		return
	end

	err,ret = myexec("sudo /usr/bin/smbpasswd -x "..qp.user_name)
	if ret ~= 0 then ui_message_err(err) return end
	if qp.delete_sh then
		err,ret = myexec("sudo /usr/sbin/userdel "..qp.user_name)
	--	err,ret = myexec("sudo /usr/sbin/userdel -r "..qp.user_name)
	else
		err,ret = myexec("sudo /usr/sbin/userdel "..qp.user_name)
	end
	if ret ~= 0 then ui_message_err(err) return end

	ui_message_success("删除用户\""..qp.user_name)
end

display()
