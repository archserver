#!/usr/bin/lua

dofile "libutil.lua"

local vgname = "sharevg"
local lvname = "homelv"

function no_lv()
				print([[
						<h3>共享状态</h3>
						<br> <font ><strong>您的系统上还没有共享卷。</strong></font>
						<hr>]])

				ui_message_warning([[
				<strong>共享卷</strong>是一个虚拟的磁盘，所有共享都会放在这个磁盘上，以方便管理。共享卷是由一个以上的硬盘或Raid组成的大的磁盘.
				<a href="volumes_create_sharelv.cgi?todo=disks">点击此处创建共享卷</a>
			</p>]])
end

function display()
	local shares = smbshare.get()
	local options = "<option id=_select style=\"color:blue;\" value=myselect selected>------ 共享列表 ------</option>"
	local hiddens = [[<input type=hidden id="myselect">]]
	local style = [[style="background-image:url(icons/volume.png);"]]
		
	for s in pairs(shares) do
		if s ~= "global" then
			if not shares[s].comment then shares[s].comment="" end
			if not shares[s].path then shares[s].path="" end
			options = options..[[<option ]]..style..[[ value="]]..s..[[">]]..s..[[</option>]]
			hiddens = hiddens..[[<input type=hidden id="]]..s..[[" value="]]..s.."|"..shares[s].path.."|"..shares[s].comment..[[">]]
		end
	end

		print([[
			<form action="shares_edit_share.cgi",method="post">
			<table width="80%"><tr><td>
			<select class="icon-menu" name="selTitle" size=10 id="titles" onchange="setOtherText(this)" width="100%"> 
			]]
			..options..
			[[
			</select>
			</td>
			<td>
				<div align="left">
				<table width="90%" cellspacing="2" cellpadding="5" border="0">
				<tr class=color_table_row1><td>
					<strong>共享名称</strong>
					<input type="text" id="share" name="share"> 
				</td></tr>
				<tr class=color_table_row2><td>
					<strong>共享目录</strong>
					<input type="text" id="path" name="path"> 
				</td></tr>
				<tr class=color_table_row1><td>
					<strong>共享描述 </strong>
					<input type="text" id="comment" name="comment"> 
				</td></tr>
				<tr><td>
				<input type=hidden name=todo value="">
				<input type="submit" value="新建共享" onclick="return mysubmit(1)">
				<input type="submit" value="更新共享" onclick="return mysubmit(2)">
				<input type="submit" value="删除共享" onclick="return mysubmit(3)">
				</td></tr>
				</table>
				</div>
			</td></tr>
			</table>
			]]
			..hiddens..
			[[
			</form>

			<script> 
			function setOtherText(obj) { 
				if (!document.layers) { 
					var share = document.getElementById("share"); 
					var path = document.getElementById("path"); 
					var comment = document.getElementById("comment"); 
					var str = document.getElementById(obj.value).value;
					var arr = str.split("|");
					share.value = arr[0];
					if (arr[1] == null){
						path.value = "";
					} else
						path.value = arr[1];
					if (arr[2] == null){
						comment.value = "";
					}else
						comment.value = arr[2];
				} 
			} 

			function mysubmit(value) {
					if (value == 1){
							document.forms[0].todo.value="edit_create";
					}else if (value == 2) {
							document.forms[0].todo.value="edit_update";
					} else if (value == 3) {
							var ss = "您要删除共享\""+document.getElementById("share").value + "\"吗?";
							if (confirm(ss)) {
								document.forms[0].todo.value="delete";
								document.forms[0].action="shares.cgi";
							} else {
								return false;
							}
						}
					document.forms[0].submit();
			}
			</script> 

			]])
end
-------------------------
--CGI OUTPUT 
-------------------------
qp = ui_getqp()
if is_vg_exist(vgname) then
		if not qp.todo then
				print("<h3>共享管理</h3>")
				display()
		elseif qp.todo=="delete" then
			if qp.share and qp.share ~= "" then 
				local sh_list = smbshare.get()
				if not sh_list[qp.share] then 
					ui_message_err("共享 <strong>"..qp.share.."</strong>不存在<br>")
					return
				end
				smbshare.remove(qp.share)

				sh_list = smbshare.get()
				if not sh_list[qp.share] then 
					print("共享 <strong>"..qp.share.."</strong>已删除<br>")
				else
					ui_message_err("共享 <strong>"..qp.share.."</strong>删除失败.<br>")
				end
			else
				ui_message_err("请指定要删除的共享名.")
			end
		end
else
				no_lv()
end

