# makefile for archss, top manage routine
# change it if the framework need upgrade

pkg:= pkg-postfix pkg-lighttpd

all:
	@echo 'all done(make help -> for detail)'

pkg-postfix: dummy
	${MAKE} -C mail/postfix/pkg/source_postfix/
	cd mail/postfix/pkg/ && makepkg

pkg-lighttpd: dummy
	${MAKE} -C httpd/lighttpd/

help: dummy
	@echo pkg: ${pkg}

.PHONY: dummy
